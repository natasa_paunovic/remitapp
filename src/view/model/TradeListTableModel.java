/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.model;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import eu.europa.acer.remit.remittable1_v1.AnnexTable1Trade;
import eu.europa.acer.remit.remittable1_v1.ObjectFactory;
import eu.europa.acer.remit.remittable1_v1.REMITTable1;
import eu.europa.acer.remit.remittable1_v1.ReportingEntityID;
import eu.europa.acer.remit.remittable1_v1.TradeListType;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author N4T
 */
public class TradeListTableModel extends AbstractTableModel {

    ArrayList<AnnexTable1Trade> tradeList;
    String[] nameOfColumns = new String[]{"R.N.", "Id Of Market Participant", "Trader Id",
        "Other Market Participant", "Beneficiary Id", "Trading Capacity",
        "Buy Sell Indicator", "Contract Id", "Contract Name", "Contract Type",
        "Energy Commodity", "Settlement Method", "Organised Market Place Id",
        "Delivery Point Or Zone", "Delivery Start Date", "Delivery End Date",
        "Load Type", "Days Of The Week", "Start Time", "End Time", "Organised Market Place Id",
        "Transaction Time", "Unique Transaction Identifier", "Price",
        "Price Currency", "Notional Amount", "Notional Currency", "Value", "Unit", "TNCQ Value", "TNCQ Unit", "ActionType", "Linked Transaction Id"};

    public TradeListTableModel(ArrayList<AnnexTable1Trade> arrayList) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        this.tradeList = arrayList;
    }

    @Override
    public String getColumnName(int column) {
        return nameOfColumns[column];
    }

    @Override
    public int getRowCount() {
        return tradeList.size();
    }

    @Override
    public int getColumnCount() {
        return nameOfColumns.length;
    }

    public void obrisiRed(int rowIndex) {

        tradeList.remove(rowIndex);
        //when removed, i want to refresh list as i update recNumber
        ArrayList<AnnexTable1Trade> trades=new ArrayList<>(tradeList.size());
        int pok=1;
        for(int i=0; i<tradeList.size(); i++){
         AnnexTable1Trade annex=tradeList.get(i);
            System.out.println("TradeListModel"+annex.getRecordSeqNumber());
         annex.setRecordSeqNumber(new BigInteger(""+pok));
             System.out.println("TradeListModel 2"+annex.getRecordSeqNumber());
              trades.add(annex);
         pok++;
        }
       
        tradeList=trades;
        fireTableDataChanged();

    }

    public void obrisiSve() {
        tradeList.removeAll(Kontroler.getInstance().vratiSveTradeListe());
        fireTableDataChanged();
    }

    public AnnexTable1Trade vratiSelektovaniReportTrade(int rowIndex) {
        return tradeList.get(rowIndex);
    }

    public ArrayList<AnnexTable1Trade> vratiTradeList() {
        return tradeList;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        AnnexTable1Trade trade = tradeList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return trade.getRecordSeqNumber();
            case 1:
                return trade.getIdOfMarketParticipant().getAce();
            case 2:
                if (trade.getTraderID() == null) {
                    return "";
                    // return;
                } else {
                    return trade.getTraderID().getTraderIdForMarketParticipant();
                }

            case 3:
                if (trade.getOtherMarketParticipant().getAce() != null) {
                    return trade.getOtherMarketParticipant().getAce();
                } else if (trade.getOtherMarketParticipant().getBic() != null) {
                    return trade.getOtherMarketParticipant().getBic();
                } else if (trade.getOtherMarketParticipant().getEic() != null) {
                    return trade.getOtherMarketParticipant().getEic();
                } else if (trade.getOtherMarketParticipant().getGln() != null) {
                    return trade.getOtherMarketParticipant().getGln();
                } else {
                    return trade.getOtherMarketParticipant().getLei();
                }
            case 4:
                if (trade.getBeneficiaryIdentification() == null) {
                    return "";
                } else {
                    return trade.getBeneficiaryIdentification().getAce();
                }
            case 5:
                return trade.getTradingCapacity();
            case 6:
                return trade.getBuySellIndicator();
            case 7:
                if (trade.getContractInfo() == null) {
                    return "";
                } else {
                    return trade.getContractInfo().getContract().getContractId();
                }
            case 8:
                if (trade.getContractInfo().getContract() == null || trade.getContractInfo() == null || trade.getContractInfo().getContract().getContractName() == null) {
                    return "";
                } else {
                    return trade.getContractInfo().getContract().getContractName();
                }
            case 9:
                return trade.getContractInfo().getContract().getContractType();
            case 10:
                return trade.getContractInfo().getContract().getEnergyCommodity();
            case 11:
                return trade.getContractInfo().getContract().getSettlementMethod();
            case 12:
                if (trade.getContractInfo().getContract().getOrganisedMarketPlaceIdentifier().getAce() != null) {
                    return "ACER";
                } else if (trade.getContractInfo().getContract().getOrganisedMarketPlaceIdentifier().getBil() != null) {
                    return "XBIL";
                } else if (trade.getContractInfo().getContract().getOrganisedMarketPlaceIdentifier().getLei() != null) {
                    return "LEI";
                } else {
                    return "MIC";
                }

            case 13:
                return trade.getContractInfo().getContract().getDeliveryPointOrZone();
            case 14:
                return trade.getContractInfo().getContract().getDeliveryStartDate();
            case 15:
                return trade.getContractInfo().getContract().getDeliveryEndDate();
            case 16:
                return trade.getContractInfo().getContract().getLoadType();
            case 17:
                if (trade.getContractInfo().getContract().getDeliveryProfile().get(0).getDaysOfTheWeek().size() == 0) {
                    return "";
                } else {
                    return trade.getContractInfo().getContract().getDeliveryProfile().get(0).getDaysOfTheWeek().get(0);
                }
            case 18:
                return trade.getContractInfo().getContract().getDeliveryProfile().get(0).getLoadDeliveryStartTimeAndLoadDeliveryEndTime().get(0).getValue().toXMLFormat();
            case 19:
                return trade.getContractInfo().getContract().getDeliveryProfile().get(0).getLoadDeliveryStartTimeAndLoadDeliveryEndTime().get(1).getValue().toXMLFormat();
            case 20:
                if (trade.getOrganisedMarketPlaceIdentifier().getAce() != null) {
                    return "ACER";
                } else if (trade.getOrganisedMarketPlaceIdentifier().getBil() != null) {
                    return "XBIL";
                } else if (trade.getOrganisedMarketPlaceIdentifier().getLei() != null) {
                    return "LEI";
                } else {
                    return "MIC";
                }
            case 21:
                return trade.getTransactionTime();
            case 22:
                if (trade.getUniqueTransactionIdentifier() == null) {
                    return "";
                } else {
                    return trade.getUniqueTransactionIdentifier().getUniqueTransactionIdentifier();
                }
            case 23:
                return trade.getPriceDetails().getPrice();
            case 24:
                return trade.getPriceDetails().getPriceCurrency();
            case 25:
                return trade.getNotionalAmountDetails().getNotionalAmount();
            case 26:
                return trade.getNotionalAmountDetails().getNotionalCurrency();
            case 27:
                return trade.getQuantity().getValue();
            case 28:
                return trade.getQuantity().getUnit();
            case 29:
                return trade.getTotalNotionalContractQuantity().getValue();
            case 30:
                return trade.getTotalNotionalContractQuantity().getUnit();
            case 31:
                return trade.getActionType();
            case 32:
                  if (trade.getLinkedTransactionId() == null) {
                    return "";
                } else {
                   if(trade.getLinkedTransactionId().isEmpty()){
                       return "";
                   } else{
                   return trade.getLinkedTransactionId().get(0);
                   }
                   
                }
            default:
                return "n/a";

        }

    }

    public void dodajTradeListUTabelu(AnnexTable1Trade a) {
        tradeList.add(a);
        //za ponovno iscrtavanje tabele, refresh,obavestenje da tabela treba da se osvezi

        fireTableDataChanged();
    }

}
