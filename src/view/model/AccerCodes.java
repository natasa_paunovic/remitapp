/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.model;

/**
 *
 * @author N4T
 */
public class AccerCodes {

    public static final String ELPETRA_BG = "A0001292D.BG";
    public static final String KOZLODUY = "A0004993U.BG";
    public static final String ELPETRA_CY = "A0006121H.CY";
    public static final String PPC = "A0003522L.GR";
    public static final String HERON = "A0003851E.GR";
    public static final String ELPEDISON = "A0002988W.GR";
    public static final String PROTERGIA = "A0004237F.GR";
    public static final String EDF = "A0000272H.HU";
    public static final String EUROPE_ENERGY = "A00012868.IT";
    public static final String EDS_RO = "A00016094.RO";
    public static final String AYEN = "A0004159H.SI";
    public static final String EDS_SK = "A0001784B.SK";
    public static final String SEE_POWER = "A0004627Z.BG";
    public static final String TPP_MARITSA = "A00033664.BG";
    public static final String TRANSENERGO = "A00028362.RO";
    public static final String NEAS_ENERGY_DK = "A0000005B.DK";
    public static final String PETROL_SI = "A0002229K.SI";
    public static final String ENERGANA_CZ = "A0004347Z.CZ";
    public static final String ENERGO_PRO_BG ="A0003201M.BG";
    public static final String EDS_HR="A0008993Y.HR";

    
    String companyName;
    String accerCode;

    public AccerCodes() {
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAccerCode() {
        return accerCode;
    }

    public void setAccerCode(String accerCode) {
        this.accerCode = accerCode;
    }

    @Override
    public String toString() {
        return companyName;
    }
    
    
    


}
