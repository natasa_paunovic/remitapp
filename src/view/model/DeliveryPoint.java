/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.model;

/**
 *
 * @author N4T
 */
public class DeliveryPoint {

    public static String HUNGARY = "10YHU-MAVIR----U";
    public static String BULGARIA = "10YCA-BULGARIA-R";
    public static String SLOVAKIA = "10YCB-SLOVAKIA-H";
    public static String IT_SL = "10YDOM-1001A0036";
    public static String GR_IT = "10YDOM-1010A0108";
    public static String GR_TR = "10YDOM--GR-TR--8";
    public static String GR_MK = "10YDOM-1001A053S";
    public static String HR_SL = "10YDOM-1001A0044";
    public static String HR_HU = "10YDOM--HR-HU--B";
    public static String HR_BA = "10YDOM-1001A046P";
    public static String HR_RS = "10YDOM-1010A026U";
    public static String HU_RO = "10YDOM-1001A027T";
    public static String HU_SK = "10YDOM-1001A028R";
    public static String HU_UA = "10YDOM-1001A029P";
    public static String HU_AT = "10YDOM-AT-HU---0";
    public static String HU_RS = "10YDOM-1010A0132";
    public static String BA_RS = "10YDOM-1001A0303";
    public static String ME_BA = "10YDOM-1001A045R";
    public static String ME_RS = "10YDOM-1001A034W";
    public static String ME_AL = "10YDOM-1001A054Q";
    public static String AL_GR = "10YDOM-1001A042X";
    public static String AL_RS = "10YDOM-1010A032Z";
    public static String BG_GR = "10YDOM-1001A043V";
    public static String BG_TR = "10YDOM--BG-TR--E";
    public static String BG_RS = "10YDOM-1001A033Y";
    public static String MK_RS = "10YDOM-1001A0311";
    public static String RO_RS = "10YDOM-1001A035U";
    public static String RO_UA = "10YDOM-1001A0897";
    public static String RO_BG = "10YDOM-1001A090M";
    public static String CZ_SK = "10YDOM-1001A083J";
    public static String AT_CZ = "10YDOM-AT-CZ---5";
    public static String AT_SL = "10YDOM-AT-SI---V";
    public static String APG_TENNET = "10YDOM-1010A018T";

    //public static String 
    
    String deliveryPoint;
    String codeDelivery;

    public DeliveryPoint() {
    }

    public String getDeliveryPoint() {
        return deliveryPoint;
    }

    public void setDeliveryPoint(String deliveryPoint) {
        this.deliveryPoint = deliveryPoint;
    }

    public String getCodeDelivery() {
        return codeDelivery;
    }

    public void setCodeDelivery(String codeDelivery) {
        this.codeDelivery = codeDelivery;
    }

    @Override
    public String toString() {
        return  deliveryPoint ;
    }
    
    
    
}
