/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.model;

import eu.europa.acer.remit.remittable1_v1.AnnexTable1ContractType;
import eu.europa.acer.remit.remittable1_v1.AnnexTable1Trade;
import java.math.BigInteger;
import java.util.ArrayList;

/**
 *
 * @author N4T
 */
public class Kontroler {
    
    ArrayList<AnnexTable1Trade> tradeList;
    ArrayList<DeliveryPoint> deliveryPointsList;
    ArrayList<AccerCodes> accerCodesList;
    String company;
    String deliveryPoint;
        int record=1;

   
    private Kontroler() {
        tradeList= new ArrayList<AnnexTable1Trade>();
        accerCodesList= new ArrayList<>();
        deliveryPointsList= new ArrayList<>();
       
    }
    private static Kontroler instance;

    public static Kontroler getInstance() {
        if (instance == null) {
            instance = new Kontroler();
        }
        return instance;
    }
    
      public void sacuvajReportUTabeli(AnnexTable1Trade a){
         tradeList.add(a);
         System.out.println("Kontroler"+a.getRecordSeqNumber());   
         }

     public void sacuvajAccerCodove(AccerCodes a){
      accerCodesList.add(a);
         System.out.println("Kontroler"+a.companyName+ " code"+a.accerCode);
     }  

    public ArrayList<DeliveryPoint> getDeliveryPointsList() {
        return deliveryPointsList;
    }
     
       public void sacuvajDeliveryPoints(DeliveryPoint a){
      deliveryPointsList.add(a);
         System.out.println("Kontroler"+a.deliveryPoint +" del code"+a.codeDelivery);
     }  
      
      public ArrayList<AccerCodes> getAccerCodes(){
       return accerCodesList;
      }
      
      public AnnexTable1Trade vratiTradeListuSaRecNumber(String number){
          AnnexTable1Trade anex=null;
       for(int i=0; i<tradeList.size(); i++){
         anex=tradeList.get(i);
         if(anex.getRecordSeqNumber().toString().equals(number)){
             System.out.println("vratiTradeList"+number);
         return anex;
         }
       }
       return anex;
      }
        
        public ArrayList<AnnexTable1Trade> vratiSveTradeListe (){
        return tradeList;
        }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDeliveryPoint() {
        return deliveryPoint;
    }

    public void setDeliveryPoint(String deliveryPoint) {
        this.deliveryPoint = deliveryPoint;
    }
    
    
    
    public AccerCodes getCompanyFromAcerCode(String acer){
        AccerCodes accer=null;
        for(int i=0; i<accerCodesList.size(); i++){
            accer=accerCodesList.get(i);
        if(accer.accerCode.equals(acer)){
            return accer;
        }
        }
        return accer;
    }
//        }
//           if (acer.equals(AccerCodes.AYEN)) {
//            return "AYEN SI";
//        } else if (acer.equals(AccerCodes.EDF)) {
//            return "EDF HU";
//          
//        } else if (acer.equals(AccerCodes.EDS_RO)) {
//            return "EDS RO";
//          
//        } else if (acer.equals(AccerCodes.EDS_SK)) {
//            return"EDS SK";
//          
//        } else if (acer.equals(AccerCodes.ELPEDISON)) {
//           return "ELPEDISON GR";
//         
//        } else if (acer.equals(AccerCodes.ELPETRA_BG)) {
//            return "ELPETRA BG";
//           
//        } else if (acer.equals(AccerCodes.ELPETRA_CY)) {
//            return "ELPETRA CY";
//        
//        } else if (acer.equals(AccerCodes.EUROPE_ENERGY)) {
//            return "Europe Energy IT";
//
//        } else if (acer.equals(AccerCodes.HERON)) {
//            return "HERON GR";
//
//        } else if (acer.equals(AccerCodes.KOZLODUY)) {
//          return "KOZLODUY BG";
//     
//        } else if (acer.equals(AccerCodes.PPC)) {
//            return "PPC GR";
//   
//        } else if (acer.equals(AccerCodes.PROTERGIA)) {
//            return "PROTERGIA GR";
//      
//        } else if (acer.equals(AccerCodes.SEE_POWER)) {
//            return "SEE POWER BG";
//
//        } else if (acer.equals(AccerCodes.TPP_MARITSA)) {
//            return "TPP Maritsa East 2 BG";
//    
//        } else if(acer.equals(AccerCodes.NEAS_ENERGY_DK)){
//           return "NEAS Energy DK";
//        } else if(acer.equals(AccerCodes.PETROL_SI)){
//            return "PETROL SI";
//        }
//        else if (acer.equals(AccerCodes.ENERGANA_CZ)) {
//            return "ENERGANA CZ";
//        }           
//        else if (acer.equals(AccerCodes.ENERGO_PRO_BG)) {
//            return "ENERGO PRO BG";
//        }else if (acer.equals(AccerCodes.EDS_HR)) {
//            return "EDS HR";
//            
//        }
//        else {
//           return "Transenergo BG";
//          
//        }
    
           
     public DeliveryPoint getDeliveryPointFromCode(String delivery){
                 DeliveryPoint deliveryP=null;
        for(int i=0; i<deliveryPointsList.size(); i++){
            deliveryP=deliveryPointsList.get(i);
        if(deliveryP.codeDelivery.equals(delivery)){
            return deliveryP;
        }
        }
        return deliveryP;}
//           if (delivery.equals(DeliveryPoint.HUNGARY)) {
//            return "HUNGARY";
//        } else if (delivery.equals(DeliveryPoint.IT_SL)) {
//            return "IT-SL";
//        } else if (delivery.equals(DeliveryPoint.GR_IT)) {
//            return "GR-IT";
//            } else if (delivery.equals(DeliveryPoint.GR_MK)) {
//            return "GR-MK";
//            } else if (delivery.equals(DeliveryPoint.GR_TR)) {
//            return "GR-TR";
//            } else if (delivery.equals(DeliveryPoint.AL_GR)) {
//            return "AL-GR";
//            } else if (delivery.equals(DeliveryPoint.AL_RS)) {
//            return "AL-RS";
//            } else if (delivery.equals(DeliveryPoint.AT_CZ)) {
//            return "AT-CZ";
//            } else if (delivery.equals(DeliveryPoint.AT_SL)) {
//            return "AT-SL";
//            } else if (delivery.equals(DeliveryPoint.APG_TENNET)) {
//            return "APG-TENNET";
//            } else if (delivery.equals(DeliveryPoint.HR_BA)) {
//            return "HR-BA";
//            } else if (delivery.equals(DeliveryPoint.HR_HU)) {
//            return "HR-HU";
//            } else if (delivery.equals(DeliveryPoint.HR_RS)) {
//            return "HR-RS";
//            } else if (delivery.equals(DeliveryPoint.HR_SL)) {
//            return "HR-SL";
//            } else if (delivery.equals(DeliveryPoint.HU_AT)) {
//            return "HU-AT";
//            } else if (delivery.equals(DeliveryPoint.HU_RO)) {
//            return "HU-RO";
//            } else if (delivery.equals(DeliveryPoint.HU_RS)) {
//            return "HU-RS";
//            } else if (delivery.equals(DeliveryPoint.HU_SK)) {
//            return "HU-SK";
//            } else if (delivery.equals(DeliveryPoint.HU_UA)) {
//            return "HU-UA";
//            } else if (delivery.equals(DeliveryPoint.IT_SL)) {
//            return "IT-SL";
//            } else if (delivery.equals(DeliveryPoint.ME_AL)) {
//            return "ME-AL";
//            } else if (delivery.equals(DeliveryPoint.ME_BA)) {
//            return "ME-BA";
//            } else if (delivery.equals(DeliveryPoint.ME_RS)) {
//            return "ME-RS";
//            } else if (delivery.equals(DeliveryPoint.MK_RS)) {
//            return "MK-RS";
//            } else if (delivery.equals(DeliveryPoint.RO_BG)) {
//            return "RO-BG";
//            } else if (delivery.equals(DeliveryPoint.RO_RS)) {
//            return "RO-RS";
//            } else if (delivery.equals(DeliveryPoint.RO_UA)) {
//            return "RO-UA";
//            } else if (delivery.equals(DeliveryPoint.BA_RS)) {
//            return "BA-RS";
//            } else if (delivery.equals(DeliveryPoint.BG_GR)) {
//            return "BG-GR";
//            } else if (delivery.equals(DeliveryPoint.BG_RS)) {
//            return "BG-RS";
//            } else if (delivery.equals(DeliveryPoint.BG_TR)) {
//            return "BG-TR";
//            } else if (delivery.equals(DeliveryPoint.BULGARIA)) {
//            return "BULGARIA";
//               } else if (delivery.equals(DeliveryPoint.CZ_SK)) {
//            return "CZ-SK";
//            
//        } else {
//         return "SLOVAKIA";
//        }
//     }
    
    }

//    public void updateReport(AnnexTable1Trade table) {
//      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    
//    for(AnnexTable1Trade o : tradeList) {
//   if(o.getRecordSeqNumber() == table.getRecordSeqNumber()) {
//      tradeList.remove(o);
//      tradeList.add(o);
//       System.out.println("update table"+table.getIdOfMarketParticipant().getAce());
//   }
//}
    
    
        
        
        
        
    

