/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import com.sun.org.apache.xerces.internal.parsers.IntegratedParserConfiguration;
import eu.europa.acer.remit.remittable1_v1.ActionTypesType;
import eu.europa.acer.remit.remittable1_v1.AnnexTable1ContractType;
import eu.europa.acer.remit.remittable1_v1.AnnexTable1Trade;
import eu.europa.acer.remit.remittable1_v1.BuySellIndicatorType;
import eu.europa.acer.remit.remittable1_v1.ContractInfoType1;
import eu.europa.acer.remit.remittable1_v1.ContractLoadType;
import eu.europa.acer.remit.remittable1_v1.ContractTypeType;
import eu.europa.acer.remit.remittable1_v1.CurrencyCodeType;
import eu.europa.acer.remit.remittable1_v1.DeliveryProfileDetails;
import eu.europa.acer.remit.remittable1_v1.EnergyCommodityType;
import eu.europa.acer.remit.remittable1_v1.NotionalAmountDetailsType;
import eu.europa.acer.remit.remittable1_v1.NotionalQuantityType;
import eu.europa.acer.remit.remittable1_v1.NotionalQuantityUnitType;
import eu.europa.acer.remit.remittable1_v1.ObjectFactory;
import eu.europa.acer.remit.remittable1_v1.OrganisedMarketPlaceType;
import eu.europa.acer.remit.remittable1_v1.ParticipantType;
import eu.europa.acer.remit.remittable1_v1.PriceDetailsType;
import eu.europa.acer.remit.remittable1_v1.QuantityType;
import eu.europa.acer.remit.remittable1_v1.QuantityUnitType;
import eu.europa.acer.remit.remittable1_v1.REMITTable1;
import eu.europa.acer.remit.remittable1_v1.ReportingEntityID;
import eu.europa.acer.remit.remittable1_v1.SettlementMethodType;
import eu.europa.acer.remit.remittable1_v1.TradeIdType;
import eu.europa.acer.remit.remittable1_v1.TradeListType;
import eu.europa.acer.remit.remittable1_v1.TraderIDType;
import eu.europa.acer.remit.remittable1_v1.TradingCapacityType;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import view.model.AccerCodes;
import view.model.DeliveryPoint;
import view.model.Kontroler;


/**
 *
 * @author N4T
 */
public class FormGlavna extends javax.swing.JFrame {

    static Vector headers = new Vector();
    static Vector data = new Vector();

    private static String readDateAndWrite(String toString) {

        DateFormat readFormat = new SimpleDateFormat("EE MMM dd hh:mm:ss z yyyy");
        DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat writeFormatTime = new SimpleDateFormat("HH:mm:ss");
        Date date = null;
        try {
            date = readFormat.parse(toString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String formattedDate = "";
        if (date != null) {
            if (toString.trim().endsWith("1899")) {
                formattedDate = writeFormatTime.format(date);
            } else {
                formattedDate = writeFormat.format(date);
            }
        }
        System.out.println(formattedDate);
        return formattedDate;
    }
    JFileChooser jChooser = new JFileChooser();

    /**
     * Creates new form FormGlavna
     */
    public FormGlavna() {
        initComponents();
        loadAcerCodesAndDeliveryFromExcel();
        this.setLocationRelativeTo(null);

    }

    public static String cellToString(HSSFCell cell) {
        int type;
        Object result;
        type = cell.getCellType();

        switch (type) {

            case 0: // numeric value in Excel
                if (DateUtil.isCellDateFormatted(cell)) {
                    result = readDateAndWrite(cell.getDateCellValue().toString());
                    // result = cell.getDateCellValue().toString();
                } else {
                    result = Long.toString((long) cell.getNumericCellValue());
                }

                break;
            case 1: // String Value in Excel 
                result = cell.getStringCellValue();
                break;
            case 2:
                result = cell.getCellFormula();
                break;
            case Cell.CELL_TYPE_BLANK:
                result = null;
                break;

            case Cell.CELL_TYPE_BOOLEAN:
                result = Boolean.toString(cell.getBooleanCellValue());
                break;
            default:
                throw new RuntimeException("There is no support for this type of cell");
        }

        return result.toString();
    }

    void fillData(File file) {
        int index = -1;
        HSSFWorkbook workbook = null;
        try {
            try {
                FileInputStream inputStream = new FileInputStream(file);
                workbook = new HSSFWorkbook(inputStream);
            } catch (IOException ex) {
                Logger.getLogger(FormRemitTable1.class.getName()).log(Level.SEVERE, null, ex);
            }

            String[] strs = new String[workbook.getNumberOfSheets()];
            //get all sheet names from selected workbook
            for (int i = 0; i < strs.length; i++) {
                strs[i] = workbook.getSheetName(i);
            }
            JFrame frame = new JFrame("Input Dialog");

            String selectedsheet = (String) JOptionPane.showInputDialog(
                    frame, "Which worksheet you want to import ?", "Select Worksheet",
                    JOptionPane.QUESTION_MESSAGE, null, strs, strs[0]);

            if (selectedsheet != null) {
                for (int i = 0; i < strs.length; i++) {
                    if (workbook.getSheetName(i).equalsIgnoreCase(selectedsheet)) {
                        index = i;
                    }
                }
                HSSFSheet sheet = workbook.getSheetAt(index);
                HSSFRow row = sheet.getRow(0);

                headers.clear();
                for (int i = 0; i < row.getLastCellNum(); i++) {
                    HSSFCell cell1 = row.getCell(i);
                    headers.add(cell1.toString());
                }

                data.clear();
                for (int j = 1; j < sheet.getLastRowNum() + 1; j++) {
                    Vector d = new Vector();
                    String strCellValue = null;
                    row = sheet.getRow(j);
                    int noofrows = row.getLastCellNum();
                    for (int i = 0; i < noofrows; i++) {    //To handle empty excel cells 
                        HSSFCell cell = row.getCell(i,
                                org.apache.poi.ss.usermodel.Row.CREATE_NULL_AS_BLANK);
                       // System.out.println("Before " + cell.toString());
                        strCellValue = cellToString(cell);
                        d.add(strCellValue);
                        //   System.out.println("after" +cell.toString());
                    }
                    d.add("\n");
                   // System.out.println("Import Excel:Cell to string" + d);
                    parseExcelToRemit(d);
                    data.add(d);
                }
            } else {
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public XMLGregorianCalendar stringToXMLGregorianCalendar(String s)
            throws ParseException,
            DatatypeConfigurationException {
        XMLGregorianCalendar result = null;
        Date date;
        SimpleDateFormat simpleDateFormat;

      //  System.out.println("datum" + s);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        date = simpleDateFormat.parse(s);
        String str = new SimpleDateFormat("yyyy-MM-dd").format(date);
        result = DatatypeFactory.newInstance().newXMLGregorianCalendar(str);

        return result;
    }

    public XMLGregorianCalendar stringToXMLGregorianCalendarZOne(String s)
            throws
            DatatypeConfigurationException {
        XMLGregorianCalendar result = null;
        try {

            Date date;
            SimpleDateFormat simpleDateFormat;
            GregorianCalendar gregorianCalendar;

            DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
            java.sql.Time timeValue = new java.sql.Time(formatter.parse(s).getTime());
            System.out.println(timeValue);

//        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//        date = simpleDateFormat.parse(s);
//         DateFormat formatter = DateFormat.getTimeInstance();        // time only
            String str = formatter.format(timeValue);
            result = DatatypeFactory.newInstance().newXMLGregorianCalendar(str);

        } catch (ParseException ex) {
            //JOptionPane.showMessageDialog(this, "Morate da unesete vreme u formatu 00:00:00", "Greska!", JOptionPane.ERROR_MESSAGE);
            result = DatatypeFactory.newInstance().newXMLGregorianCalendar("00:00:00");
        }
        return result;
    }

    public XMLGregorianCalendar stringToXMLTransactionDate(String s)
            throws ParseException,
            DatatypeConfigurationException {
        XMLGregorianCalendar result = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
//sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = sdf.parse(s); //prints-> Mon Sep 30 02:46:19 CST 2013
        String time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(date);

        System.out.println("Transaction time" + time);
        DateTimeFormatter f = DateTimeFormatter.ISO_DATE_TIME;
        ZonedDateTime zdt = ZonedDateTime.parse(s.trim(), f);
        result = DatatypeFactory.newInstance().newXMLGregorianCalendar(time);

        return result;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")

    private String getAcerCodeFromOtherParticipant(String acer) {
        if (acer.equals(AccerCodes.AYEN)) {
            Kontroler.getInstance().setCompany("AYEN SI");
            return acer;
        } else if (acer.equals(AccerCodes.EDF)) {
            Kontroler.getInstance().setCompany("EDF HU");
            return AccerCodes.EDF;
        } else if (acer.equals(AccerCodes.EDS_RO)) {
            Kontroler.getInstance().setCompany("EDS RO");
            return acer;
        } else if (acer.equals(AccerCodes.EDS_SK)) {
            Kontroler.getInstance().setCompany("EDS SK");
            return acer;
        } else if (acer.equals(AccerCodes.ELPEDISON)) {
            Kontroler.getInstance().setCompany("ELPEDISON GR");
            return acer;
        } else if (acer.equals(AccerCodes.ELPETRA_BG)) {
            Kontroler.getInstance().setCompany("ELPETRA BG");
            return acer;
        } else if (acer.equals(AccerCodes.ELPETRA_CY)) {
            Kontroler.getInstance().setCompany("ELPETRA CY");
            return acer;
        } else if (acer.equals(AccerCodes.EUROPE_ENERGY)) {
            Kontroler.getInstance().setCompany("Europe Energy IT");
            return acer;
        } else if (acer.equals(AccerCodes.HERON)) {
            Kontroler.getInstance().setCompany("HERON GR");
            return acer;
        } else if (acer.equals(AccerCodes.KOZLODUY)) {
            Kontroler.getInstance().setCompany("KOZLODUY BG");
            return acer;
        } else if (acer.equals(AccerCodes.PPC)) {
            Kontroler.getInstance().setCompany("PPC GR");
            return acer;
        } else if (acer.equals(AccerCodes.PROTERGIA)) {
            Kontroler.getInstance().setCompany("PROTERGIA GR");
            return acer;
        } else if (acer.equals(AccerCodes.SEE_POWER)) {
            Kontroler.getInstance().setCompany("SEE POWER BG");
            return acer;
        } else if (acer.equals(AccerCodes.TPP_MARITSA)) {
            Kontroler.getInstance().setCompany("TPP Maritsa East 2");
            return acer;
        }   else if (acer.equals(AccerCodes.NEAS_ENERGY_DK)) {
            Kontroler.getInstance().setCompany("NEAS Energy DK");
            return acer;
        }  else if (acer.equals(AccerCodes.PETROL_SI)) {
            Kontroler.getInstance().setCompany("PETROL SI");
            return acer;
        }  else if (acer.equals(AccerCodes.ENERGANA_CZ)) {
            Kontroler.getInstance().setCompany("ENERGANA CZ");
            return acer;
        }           else if (acer.equals(AccerCodes.ENERGO_PRO_BG)) {
            Kontroler.getInstance().setCompany("ENERGO PRO BG");
            return acer;
        }else if (acer.equals(AccerCodes.EDS_HR)) {
            Kontroler.getInstance().setCompany("EDS HR");
            return acer;
        } else {
            Kontroler.getInstance().setCompany("Transenergo BG");
            return acer;
        }

    }

    private void parseExcelToRemit(Vector d) throws ParseException, DatatypeConfigurationException {
        String regex = "\\[|\\]";

        String annex = d.toString().replaceAll(regex, "");
        System.out.println(annex);
        String[] annexArray = annex.split(",");

        ObjectFactory factory = new ObjectFactory();
        ReportingEntityID entityID = factory.createReportingEntityID();
        entityID.setAce("B0000450I.HU");
        
        int recordNumber=0;
        //if(Kontroler.getInstance().vratiSveTradeListe()==null){

        recordNumber=Kontroler.getInstance().vratiSveTradeListe().size();
        recordNumber++;
//            System.out.println("rec" +recordNumber);
            for(int i=0; i<Kontroler.getInstance().vratiSveTradeListe().size();i++){
            if(Kontroler.getInstance().vratiSveTradeListe().get(i).getRecordSeqNumber().toString().equals(annexArray[0])){
             recordNumber = Kontroler.getInstance().vratiSveTradeListe().get(i).getRecordSeqNumber().intValue();
             recordNumber++;
                System.out.println("llllllllllllllllll"+recordNumber);
               
           // }
          
        }
               annexArray[0]=""+recordNumber;
        }
         
        

        AnnexTable1Trade annexTable = factory.createAnnexTable1Trade();

        for (int i = 0; i < annexArray.length; i++) {
            annexTable.setRecordSeqNumber(new BigInteger(annexArray[0]));
            ParticipantType type = factory.createParticipantType();

            type.setAce(annexArray[1].trim());
            annexTable.setIdOfMarketParticipant(type);

            TraderIDType traderIdType = factory.createTraderIDType();
            traderIdType.setTraderIdForMarketParticipant(annexArray[2].trim());
            annexTable.setTraderID(traderIdType);

            //acer code with company name
            ParticipantType participantType = factory.createParticipantType();
            if (annexArray[3].trim().length() == 12) {
                participantType.setAce(getAcerCodeFromOtherParticipant(annexArray[3].trim()));

            } else if (annexArray[3].trim().length() == 20) {
                participantType.setLei(annexArray[3].trim());
            } else if (annexArray[3].trim().length() == 11) {
                participantType.setBic(annexArray[3].trim());
            } else if (annexArray[3].trim().length() == 16) {
                participantType.setEic(annexArray[3].trim());
            } else {
                participantType.setGln(annexArray[3].trim());
            }
            annexTable.setOtherMarketParticipant(participantType);

            //beneficiary id
            ParticipantType type2 = factory.createParticipantType();
            type2.setAce(annexArray[4].trim());
            annexTable.setBeneficiaryIdentification(type2);

            String tradingItem = annexArray[5].trim();
            if (tradingItem.toLowerCase().equals("p")) {
                annexTable.setTradingCapacity(TradingCapacityType.P);
            } else {
                annexTable.setTradingCapacity(TradingCapacityType.A);
            }

            String sellingInd = annexArray[6].trim();
            if (sellingInd.toUpperCase().equals("B")) {
                annexTable.setBuySellIndicator(BuySellIndicatorType.B);
            } else if (sellingInd.toUpperCase().equals("C")) {
            } else {
                annexTable.setBuySellIndicator(BuySellIndicatorType.S);
            }

            AnnexTable1ContractType contractInfo = factory.createAnnexTable1ContractType();
            if (annexArray[7].trim() == null) {
                contractInfo.setContractId("");
            } else {
                contractInfo.setContractId(annexArray[7].trim());
            }
            System.out.println("contract name" + annexArray[8].trim());
            if (annexArray[8].trim() == null) {
                contractInfo.setContractName("");
            } else {
                contractInfo.setContractName(annexArray[8].trim());
            }

            String energyCommodityType = annexArray[10].trim();
            if (energyCommodityType.equals("EL")) {
                contractInfo.getEnergyCommodity().add(EnergyCommodityType.EL);
            } else {
                contractInfo.getEnergyCommodity().add(EnergyCommodityType.NG);
            }
            String contractType = annexArray[9].trim();
            if (contractType.equals("AU")) {
                contractInfo.setContractType(ContractTypeType.AU);
            } else if (contractType.equals("CO")) {
                contractInfo.setContractType(ContractTypeType.CO);
            } else if (contractType.equals("FW")) {
                contractInfo.setContractType(ContractTypeType.FW);
            } else if (contractType.equals("OP")) {
                contractInfo.setContractType(ContractTypeType.OP);
            } else if (contractType.equals("OP_FW")) {
                contractInfo.setContractType(ContractTypeType.OP_FW);
            } else if (contractType.equals("OP_FU")) {
                contractInfo.setContractType(ContractTypeType.OP_FU);
            } else if (contractType.equals("OP_SW")) {
                contractInfo.setContractType(ContractTypeType.OP_SW);
            } else if (contractType.equals("SP")) {
                contractInfo.setContractType(ContractTypeType.SP);
            } else if (contractType.equals("SW")) {
                contractInfo.setContractType(ContractTypeType.SW);
            } else if (contractType.equals("OT")) {
                contractInfo.setContractType(ContractTypeType.OT);
            } else {
                contractInfo.setContractType(ContractTypeType.FU);
            }
//
            String settlementItem = annexArray[11].trim();
            if (settlementItem.toUpperCase().equals("P")) {
                contractInfo.setSettlementMethod(SettlementMethodType.P);
            } else if (settlementItem.toUpperCase().equals("C")) {
                contractInfo.setSettlementMethod(SettlementMethodType.C);
            } else {
                contractInfo.setSettlementMethod(SettlementMethodType.O);
            }
//
            OrganisedMarketPlaceType marketPlaceType = factory.createOrganisedMarketPlaceType();
            OrganisedMarketPlaceType marketPlaceType1 = factory.createOrganisedMarketPlaceType();
            String marketPlaceItem = annexArray[12].trim();

            if (marketPlaceItem.toLowerCase().equals("lei")) {
                marketPlaceType.setLei("LEI");
            } else if (marketPlaceItem.toLowerCase().equals("mic")) {
                marketPlaceType.setMic("MIC");
            } else if (marketPlaceItem.toLowerCase().equals("acer")) {
                marketPlaceType.setAce("ACER");
            } else {
                marketPlaceType.setBil("XBIL");
            }
            contractInfo.setOrganisedMarketPlaceIdentifier(marketPlaceType);
//
            String deliveryPoint = annexArray[13].trim();
            contractInfo.getDeliveryPointOrZone().add(deliveryPoint);

//
            XMLGregorianCalendar xmlDate2 = stringToXMLGregorianCalendar(annexArray[14].trim());
            contractInfo.setDeliveryStartDate(xmlDate2);

            XMLGregorianCalendar xmlDate3 = stringToXMLGregorianCalendar(annexArray[15].trim());
            contractInfo.setDeliveryEndDate(xmlDate3);
//
            String loadItem = annexArray[16].trim();
            if (loadItem.toUpperCase().equals("BL")) {
                contractInfo.setLoadType(ContractLoadType.BL);
            } else if (loadItem.toUpperCase().equals("PL")) {
                contractInfo.setLoadType(ContractLoadType.PL);
            } else if (loadItem.toUpperCase().equals("OP")) {
                contractInfo.setLoadType(ContractLoadType.OP);
            } else if (loadItem.toUpperCase().equals("BH")) {
                contractInfo.setLoadType(ContractLoadType.BH);
            } else if (loadItem.toUpperCase().equals("SH")) {
                contractInfo.setLoadType(ContractLoadType.SH);
            } else if (loadItem.toUpperCase().equals("GD")) {
                contractInfo.setLoadType(ContractLoadType.GD);
            } else {
                contractInfo.setLoadType(ContractLoadType.OT);
            }
//
            DeliveryProfileDetails deliveryProfile = factory.createDeliveryProfileDetails();

            XMLGregorianCalendar startDate1 = stringToXMLGregorianCalendarZOne(annexArray[18].trim());
            XMLGregorianCalendar end2 = stringToXMLGregorianCalendarZOne(annexArray[19].trim());
            JAXBElement<XMLGregorianCalendar> element = factory.createDeliveryProfileDetailsLoadDeliveryStartTime(startDate1);
            JAXBElement<XMLGregorianCalendar> element2 = factory.createDeliveryProfileDetailsLoadDeliveryEndTime(end2);

            deliveryProfile.getLoadDeliveryStartTimeAndLoadDeliveryEndTime().add(element);
            deliveryProfile.getLoadDeliveryStartTimeAndLoadDeliveryEndTime().add(element2);
            if (annexArray[17].trim() != null) {
                deliveryProfile.getDaysOfTheWeek().add(annexArray[17].trim());
            } else {
                deliveryProfile.getDaysOfTheWeek().add("");
            }
            contractInfo.getDeliveryProfile().add(deliveryProfile);
//
            // if(!tfTransactionTime.getText().isEmpty()){
            XMLGregorianCalendar transactionTime = stringToXMLTransactionDate(annexArray[21].trim());
            annexTable.setTransactionTime(transactionTime);

            TradeIdType tradeId = factory.createTradeIdType();
            if (annexArray[22].trim() != null) {
                tradeId.setUniqueTransactionIdentifier(annexArray[22].trim());
                annexTable.setUniqueTransactionIdentifier(tradeId);
            } else {
                tradeId.setUniqueTransactionIdentifier("");
                annexTable.setUniqueTransactionIdentifier(tradeId);
            }
//
            String marketPlaceItem1 = annexArray[20].trim();

            if (marketPlaceItem1.toLowerCase().equals("lei")) {
                marketPlaceType1.setLei("LEI");
            } else if (marketPlaceItem1.toLowerCase().equals("mic")) {
                marketPlaceType1.setMic("MIC");
            } else if (marketPlaceItem1.toLowerCase().equals("acer")) {
                marketPlaceType1.setAce("ACER");
            } else {
                marketPlaceType1.setBil("XBIL");
            }

            annexTable.setOrganisedMarketPlaceIdentifier(marketPlaceType1);
//
            String actionType = annexArray[31].trim();
            if (actionType.toLowerCase().equals("e")) {
                annexTable.setActionType(ActionTypesType.E);
            } else if (actionType.toLowerCase().equals("c")) {
                annexTable.setActionType(ActionTypesType.C);
            } else if (actionType.toLowerCase().equals("m")) {
                annexTable.setActionType(ActionTypesType.M);
            } else {
                annexTable.setActionType(ActionTypesType.N);
            }
//
            PriceDetailsType priceType = factory.createPriceDetailsType();
            priceType.setPrice(new BigDecimal(annexArray[23].trim()));
//
            String priceCurrencyType = annexArray[24].trim();
            if (priceCurrencyType.toUpperCase().equals("EUR")) {
                priceType.setPriceCurrency(CurrencyCodeType.EUR);
            } else if (priceCurrencyType.toUpperCase().equals("BGN")) {
                priceType.setPriceCurrency(CurrencyCodeType.BGN);
            } else if (priceCurrencyType.toUpperCase().equals("CHF")) {
                priceType.setPriceCurrency(CurrencyCodeType.CHF);
            } else if (priceCurrencyType.toUpperCase().equals("CZK")) {
                priceType.setPriceCurrency(CurrencyCodeType.CZK);
            } else if (priceCurrencyType.toUpperCase().equals("DKK")) {
                priceType.setPriceCurrency(CurrencyCodeType.DKK);
            } else if (priceCurrencyType.toUpperCase().equals("EUX")) {
                priceType.setPriceCurrency(CurrencyCodeType.EUX);
            } else if (priceCurrencyType.toUpperCase().equals("GBP")) {
                priceType.setPriceCurrency(CurrencyCodeType.GBP);
            } else if (priceCurrencyType.toUpperCase().equals("GBX")) {
                priceType.setPriceCurrency(CurrencyCodeType.GBX);
            } else if (priceCurrencyType.toUpperCase().equals("HRK")) {
                priceType.setPriceCurrency(CurrencyCodeType.HRK);
            } else if (priceCurrencyType.toUpperCase().equals("HUF")) {
                priceType.setPriceCurrency(CurrencyCodeType.HUF);
            } else if (priceCurrencyType.toUpperCase().equals("ISK")) {
                priceType.setPriceCurrency(CurrencyCodeType.ISK);
            } else if (priceCurrencyType.toUpperCase().equals("NOK")) {
                priceType.setPriceCurrency(CurrencyCodeType.NOK);
            } else if (priceCurrencyType.toUpperCase().equals("PCT")) {
                priceType.setPriceCurrency(CurrencyCodeType.PCT);
            } else if (priceCurrencyType.toUpperCase().equals("PLN")) {
                priceType.setPriceCurrency(CurrencyCodeType.PLN);
            } else if (priceCurrencyType.toUpperCase().equals("RON")) {
                priceType.setPriceCurrency(CurrencyCodeType.RON);
            } else if (priceCurrencyType.toUpperCase().equals("SEK")) {
                priceType.setPriceCurrency(CurrencyCodeType.SEK);
            } else {
                priceType.setPriceCurrency(CurrencyCodeType.USD);
            }

            annexTable.setPriceDetails(priceType);
//
            NotionalAmountDetailsType notionalDetailsType = factory.createNotionalAmountDetailsType();
            notionalDetailsType.setNotionalAmount(new BigDecimal(annexArray[25].trim()));
//
            String notionalCurrency = annexArray[26].trim();
            if (notionalCurrency.toUpperCase().equals("EUR")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.EUR);
            } else if (notionalCurrency.toUpperCase().equals("BGN")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.BGN);
            } else if (notionalCurrency.toUpperCase().equals("CHF")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.CHF);
            } else if (notionalCurrency.toUpperCase().equals("CZK")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.CZK);
            } else if (notionalCurrency.toUpperCase().equals("DKK")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.DKK);
            } else if (notionalCurrency.toUpperCase().equals("EUX")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.EUX);
            } else if (notionalCurrency.toUpperCase().equals("GBP")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.GBP);
            } else if (notionalCurrency.toUpperCase().equals("GBX")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.GBX);
            } else if (notionalCurrency.toUpperCase().equals("HRK")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.HRK);
            } else if (notionalCurrency.toUpperCase().equals("HUF")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.HUF);
            } else if (notionalCurrency.toUpperCase().equals("ISK")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.ISK);
            } else if (notionalCurrency.toUpperCase().equals("NOK")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.NOK);
            } else if (notionalCurrency.toUpperCase().equals("PCT")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.PCT);
            } else if (notionalCurrency.toUpperCase().equals("PLN")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.PLN);
            } else if (notionalCurrency.toUpperCase().equals("RON")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.RON);
            } else if (notionalCurrency.toUpperCase().equals("SEK")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.SEK);
            } else {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.USD);
            }
            annexTable.setNotionalAmountDetails(notionalDetailsType);
//
            QuantityType quantityType = factory.createQuantityType();
            quantityType.setValue(new BigDecimal(annexArray[27].trim()));
//
            String quantityUnit = annexArray[28].trim();
            if (quantityUnit.equals("KW")) {
                quantityType.setUnit(QuantityUnitType.KW);
            } else if (quantityUnit.equals("MW")) {
                quantityType.setUnit(QuantityUnitType.MW);
            } else if (quantityUnit.equals("cm/d")) {
                quantityType.setUnit(QuantityUnitType.CM_D);
            } else if (quantityUnit.equals("GW")) {
                quantityType.setUnit(QuantityUnitType.GW);
            } else if (quantityUnit.equals("GWh/d")) {
                quantityType.setUnit(QuantityUnitType.G_WH_D);
            } else if (quantityUnit.equals("GWh/h")) {
                quantityType.setUnit(QuantityUnitType.G_WH_H);
            } else if (quantityUnit.equals("KTherm/d")) {
                quantityType.setUnit(QuantityUnitType.K_THERM_D);
            } else if (quantityUnit.equals("KWh/d")) {
                quantityType.setUnit(QuantityUnitType.K_WH_D);
            } else if (quantityUnit.equals("KWh/h")) {
                quantityType.setUnit(QuantityUnitType.K_WH_H);
            } else if (quantityUnit.equals("mcm/d")) {
                quantityType.setUnit(QuantityUnitType.MCM_D);
            } else if (quantityUnit.equals("MTherm/d")) {
                quantityType.setUnit(QuantityUnitType.M_THERM_D);
            } else if (quantityUnit.equals("MWh/d")) {
                quantityType.setUnit(QuantityUnitType.M_WH_D);
            } else if (quantityUnit.equals("MWh/h")) {
                quantityType.setUnit(QuantityUnitType.M_WH_H);
            } else {
                quantityType.setUnit(QuantityUnitType.THERM_D);
            }
            annexTable.setQuantity(quantityType);
//
            NotionalQuantityType totalNotionalType = factory.createNotionalQuantityType();
            totalNotionalType.setValue(new BigDecimal(annexArray[29].trim()));

            String tquantityUnit = annexArray[30].trim();
            if (tquantityUnit.equals("cm")) {
                totalNotionalType.setUnit(NotionalQuantityUnitType.CM);
            } else if (tquantityUnit.equals("GWh")) {
                totalNotionalType.setUnit(NotionalQuantityUnitType.G_WH);
            } else if (tquantityUnit.equals("KTherm")) {
                totalNotionalType.setUnit(NotionalQuantityUnitType.K_THERM);
            } else if (tquantityUnit.equals("KWh")) {
                totalNotionalType.setUnit(NotionalQuantityUnitType.K_WH);
            } else if (tquantityUnit.equals("mcm")) {
                totalNotionalType.setUnit(NotionalQuantityUnitType.MCM);
            } else if (tquantityUnit.equals("MTherm")) {
                totalNotionalType.setUnit(NotionalQuantityUnitType.M_THERM);
            } else if (tquantityUnit.equals("Therm")) {
                totalNotionalType.setUnit(NotionalQuantityUnitType.THERM);
            } else {
                totalNotionalType.setUnit(NotionalQuantityUnitType.M_WH);
            }
            annexTable.setTotalNotionalContractQuantity(totalNotionalType);
//
            ContractInfoType1 contractInfoType1 = factory.createContractInfoType1();
            contractInfoType1.setContract(contractInfo);
            annexTable.setContractInfo(contractInfoType1);
//
            TradeListType tradeList = factory.createTradeListType();
            tradeList.getTradeReport().add(annexTable);
        }
        Kontroler.getInstance().sacuvajReportUTabeli(annexTable);

    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Remit Application");
        setName("RemitTable1"); // NOI18N
        setPreferredSize(new java.awt.Dimension(227, 339));
        setResizable(false);

        jButton1.setText("Insert Standard Contract");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton3.setText("Insert Report Table");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/excelpng.png"))); // NOI18N
        jButton4.setText("Import From Excel");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButton2.setText("Close");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        PanelRemitTable1 panel = new PanelRemitTable1();
        JDialog dialog = new JDialog(this, "Insert new trade report", true);
        dialog.add(panel);
        dialog.pack();
        //dialog.setBounds(100, 100, 250, 200);
        dialog.setVisible(true);
        dialog.addWindowListener(panel);


    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        FormRemitTable1 form = new FormRemitTable1(this);
        form.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        jChooser.showOpenDialog(null);
        jChooser.setDialogTitle("Select only Excel workbooks");
        File file = jChooser.getSelectedFile();
        if (file == null) {
            JOptionPane.showMessageDialog(
                    null, "Please select any Excel file",
                    "Help",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        } else if (!file.getName().endsWith("xls")) {
            JOptionPane.showMessageDialog(
                    null, "Please select only Excel file.",
                    "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            fillData(file);
            FormRemitTable1 form = new FormRemitTable1(this);
            form.setVisible(true);
//                                            model = new DefaultTableModel(data, headers); 
//                                            tableWidth = model.getColumnCount() * 150; 
//                                            tableHeight = model.getRowCount() * 25; 
//                                            table.setPreferredSize(new Dimension( tableWidth, tableHeight));
//                                             table.setModel(model); 
        }

    }//GEN-LAST:event_jButton4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormGlavna.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormGlavna.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormGlavna.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormGlavna.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormGlavna().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

    private boolean isTime(String tostring) {
        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //   boolean isTime=false;
        if (tostring.trim().contains(":")) {
            System.out.println("is time" + tostring);
            return true;
        } else {
            return false;
        }
    }

    private void loadAcerCodesAndDeliveryFromExcel() {

        XSSFWorkbook workbook = null;

        try {

            File f = new File("ACER codes + Delivery Point.xlsx");
            System.out.println(f.getAbsolutePath());

            FileInputStream inputStream = new FileInputStream(f);

            workbook = new XSSFWorkbook(inputStream);
        } catch (IOException ex) {
            Logger.getLogger(FormGlavna.class.getName()).log(Level.SEVERE, null, ex);
        }

        String[] strs = new String[workbook.getNumberOfSheets()];
        //get all sheet names from selected workbook
        for (int i = 0; i < strs.length; i++) {
            strs[i] = workbook.getSheetName(i);
        }

        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFRow row = sheet.getRow(0);

        for (int j = 1; j < sheet.getLastRowNum() + 1; j++) {
            Vector d = new Vector();
            String strCellValue = null;
            row = sheet.getRow(j);

            int noofrows = row.getLastCellNum();
            for (int i = 0; i < noofrows; i++) {    //To handle empty excel cells 
                XSSFCell cell = row.getCell(i,
                        org.apache.poi.ss.usermodel.Row.CREATE_NULL_AS_BLANK);
                // System.out.println("Before " + cell.toString());
                strCellValue = cellToStringXSExcel(cell);
                d.add(strCellValue);

                //   System.out.println("after" +cell.toString());
            }
            //   d.add("\n:");
            parseExcelToKontroler(d);
            //  System.out.println("Import Excel MernaMesta:Cell to string" + d);

        }
    
    }

    private void parseExcelToKontroler(Vector d) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
     String regex = "\\[|\\]";
        String ocitavanja = d.toString().replaceAll(regex, "");

        AccerCodes code= new AccerCodes();
        DeliveryPoint point= new DeliveryPoint();
        String[] accerAndDeliveryArray = ocitavanja.split(",");
        for(int i=0; i<accerAndDeliveryArray.length; i++){
            if(accerAndDeliveryArray[0].trim().equals("LE TRADING")){
            return;
            }

            
            if(!accerAndDeliveryArray[0].trim().isEmpty()){
        code.setCompanyName(accerAndDeliveryArray[0].trim()+ " "+accerAndDeliveryArray[1].trim());
        code.setAccerCode(accerAndDeliveryArray[2].trim());
            }
            
        point.setCodeDelivery(accerAndDeliveryArray[5].trim());
        point.setDeliveryPoint(accerAndDeliveryArray[4].trim());
        
        
        
        }
        if(code.getAccerCode() !=null && code.getCompanyName()!=null){
        Kontroler.getInstance().sacuvajAccerCodove(code);}
        Kontroler.getInstance().sacuvajDeliveryPoints(point);
    
    }

    private String cellToStringXSExcel(XSSFCell cell) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
 
        //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        int type;
        String result;
        type = cell.getCellType();

        switch (type) {

            case 0: // numeric value in Excel
                if (DateUtil.isCellDateFormatted(cell)) {
                    result = readDateAndWrite(cell.getDateCellValue().toString());
                    // result = cell.getDateCellValue().toString();
                } else {

                    String result2 = "" + cell.getNumericCellValue();
                    if (result2.contains("E7") || result2.contains("E9") || result2.contains("E12") || result2.contains("E8") || result2.contains("E10") || result2.contains("E11") || result2.contains("E13")) {
                        result = "" + (long) cell.getNumericCellValue();
                        //System.out.println("result xlsx" + result);
                    } else {
                        result = result2;
                    }
                }

                break;
            case 1: // String Value in Excel 

                result = cell.getStringCellValue();
                break;
            case 2:
                result = "" + cell.getNumericCellValue();
                if (result.contains("E7") || result.contains("E9") || result.contains("E12") || result.contains("E8") || result.contains("E10") || result.contains("E13")) {
                    result = "" + (long) cell.getNumericCellValue();
                    //System.out.println("result xlsx" + result);
                }
                //cell.getCellFormula();
                break;
            case Cell.CELL_TYPE_BLANK:
                result = "";
                break;

            case Cell.CELL_TYPE_BOOLEAN:
                result = Boolean.toString(cell.getBooleanCellValue());
                break;
            default:
                throw new RuntimeException("There is no support for this type of cell");
        }

        return result;

    }
    
    
}
