/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import eu.europa.acer.remit.remittable1_v1.ActionTypesType;
import eu.europa.acer.remit.remittable1_v1.AnnexTable1ContractType;
import eu.europa.acer.remit.remittable1_v1.AnnexTable1Trade;
import eu.europa.acer.remit.remittable1_v1.BuySellIndicatorType;
import eu.europa.acer.remit.remittable1_v1.ContractInfoType1;
import eu.europa.acer.remit.remittable1_v1.ContractLoadType;
import eu.europa.acer.remit.remittable1_v1.ContractTypeType;
import eu.europa.acer.remit.remittable1_v1.CurrencyCodeType;
import eu.europa.acer.remit.remittable1_v1.DeliveryProfileDetails;
import eu.europa.acer.remit.remittable1_v1.EnergyCommodityType;
import eu.europa.acer.remit.remittable1_v1.NotionalAmountDetailsType;
import eu.europa.acer.remit.remittable1_v1.NotionalQuantityType;
import eu.europa.acer.remit.remittable1_v1.NotionalQuantityUnitType;
import eu.europa.acer.remit.remittable1_v1.ObjectFactory;
import eu.europa.acer.remit.remittable1_v1.OrganisedMarketPlaceType;
import eu.europa.acer.remit.remittable1_v1.ParticipantType;
import eu.europa.acer.remit.remittable1_v1.PriceDetailsType;
import eu.europa.acer.remit.remittable1_v1.QuantityType;
import eu.europa.acer.remit.remittable1_v1.QuantityUnitType;
import eu.europa.acer.remit.remittable1_v1.REMITTable1;
import eu.europa.acer.remit.remittable1_v1.ReportingEntityID;
import eu.europa.acer.remit.remittable1_v1.SettlementMethodType;
import eu.europa.acer.remit.remittable1_v1.TradeIdType;
import eu.europa.acer.remit.remittable1_v1.TradeListType;
import eu.europa.acer.remit.remittable1_v1.TraderIDType;
import eu.europa.acer.remit.remittable1_v1.TradingCapacityType;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Window;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.AbstractList;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import jdk.nashorn.internal.ir.BreakNode;
import view.model.AccerCodes;
import view.model.DeliveryPoint;
import view.model.Kontroler;

/**
 *
 * @author N4T
 */
public class PanelRemitTable1 extends javax.swing.JPanel implements WindowListener {

    /**
     * Creates new form PanelRemitTable1
     */
    FormGlavna form;
    private static int ID = 0;
    int rbr;
    AnnexTable1Trade trade;
    boolean isTableForAdded = false;

    public PanelRemitTable1() {
        initComponents();

        initCombo();
        initValues();
    }

    public PanelRemitTable1(boolean isTableAdded) {
        this.isTableForAdded = isTableAdded;
        initComponents();
        initCombo();
        initValues();
        buttonGenerateXML.setText("Add report");

    }

    public PanelRemitTable1(FormGlavna form) {
        initComponents();
        initCombo();
        initValues();

    }

    public PanelRemitTable1(AnnexTable1Trade tab) {
        initComponents();
        initCombo();
    }

    public void setRbr() {
        rbr = ++ID;
    }

    public void setCloseRbr() {
        rbr = --ID;
    }

    public int getRbr() {
        return rbr;
    }

    public static boolean isDouble(String s) {
        try {
            Double.parseDouble(s);
            System.out.println("double");

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isInteger(String s) {
        try {
            Double.parseDouble(s);
            System.out.println("integer");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        tfRecordNumber = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        tfBeneficiaryId = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        cbTradingCapacity = new javax.swing.JComboBox<>();
        cbMarketParticipant = new javax.swing.JComboBox<>();
        tfOtherMarketParticipant = new javax.swing.JTextField();
        cbBuysellIndicator = new javax.swing.JComboBox<>();
        cbOrganizedMarketPlace = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        tfContractId = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        cbContractType = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        cbEnergyCommodity = new javax.swing.JComboBox<>();
        cbSettlementMethod = new javax.swing.JComboBox<>();
        cbOrganisedMarketPlace = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        jcalStartDate = new com.toedter.calendar.JDateChooser();
        jCalEnddate = new com.toedter.calendar.JDateChooser();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        tfStartTime = new javax.swing.JTextField();
        tfEndTime = new javax.swing.JTextField();
        cbDaysOfWeek = new javax.swing.JComboBox<>();
        cbLoadType = new javax.swing.JComboBox<>();
        cbContractName = new javax.swing.JComboBox<>();
        cbDeliveryPoint = new javax.swing.JComboBox<>();
        jLabel20 = new javax.swing.JLabel();
        tfTransactionTime = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        tfUniqueTransId = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        tfPrice = new javax.swing.JTextField();
        cbCurrency = new javax.swing.JComboBox<>();
        jPanel4 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        tfNotionalAmount = new javax.swing.JTextField();
        tfNotionalCurrency = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        cbActionType = new javax.swing.JComboBox<>();
        jPanel6 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        tfQuantityValue = new javax.swing.JTextField();
        cbQuantityUnit = new javax.swing.JComboBox<>();
        jPanel7 = new javax.swing.JPanel();
        tfTotalNValue = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        cbTotNotionalUnit = new javax.swing.JComboBox<>();
        jLabel30 = new javax.swing.JLabel();
        buttonGenerateXML = new javax.swing.JButton();
        jLabel31 = new javax.swing.JLabel();
        tfTraderForMarketParticipant = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        cbOrganisedMarketPlace1 = new javax.swing.JComboBox<>();
        butUpdate = new javax.swing.JButton();
        cbAcerCodes = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        tfLinkedTransactionId = new javax.swing.JTextField();

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Trade Report"));

        jLabel2.setText("Record Seq Number:");

        jLabel4.setText("Other Market Participant:");

        jLabel5.setText("Beneficiary identification:");

        jLabel6.setText("Trading Capacity:");

        jLabel7.setText("Buy Sell Indicator:");

        cbTradingCapacity.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        cbMarketParticipant.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbMarketParticipant.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbMarketParticipantItemStateChanged(evt);
            }
        });

        cbBuysellIndicator.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        cbOrganizedMarketPlace.setBorder(javax.swing.BorderFactory.createTitledBorder("Contract Info"));

        jLabel1.setText("ContractId:");

        jLabel8.setText("Contract Name:");

        jLabel9.setText("Contract Type:");

        cbContractType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel10.setText("Energy commodity:");

        jLabel11.setText("Settlement Method:");

        jLabel12.setText("Organised Market Place ID:");

        cbEnergyCommodity.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        cbSettlementMethod.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        cbOrganisedMarketPlace.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel13.setText("Delivery point or Zone:");

        jcalStartDate.setDateFormatString("yyyy-MM-dd");

        jCalEnddate.setDateFormatString("yyyy-MM-dd");

        jLabel14.setText("Delivery Start Date:");

        jLabel15.setText("Delivery End Date:");

        jLabel16.setText("Load Type:");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Delivery Profile"));

        jLabel17.setText("Days of the week:");

        jLabel18.setText("Load delivery start time:");

        jLabel19.setText("Load delivery end time:");

        cbDaysOfWeek.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbDaysOfWeek, 0, 109, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tfStartTime, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfEndTime, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(jLabel18)
                    .addComponent(jLabel19)
                    .addComponent(tfStartTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfEndTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbDaysOfWeek, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cbLoadType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbLoadType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbLoadTypeActionPerformed(evt);
            }
        });

        cbContractName.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        cbDeliveryPoint.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout cbOrganizedMarketPlaceLayout = new javax.swing.GroupLayout(cbOrganizedMarketPlace);
        cbOrganizedMarketPlace.setLayout(cbOrganizedMarketPlaceLayout);
        cbOrganizedMarketPlaceLayout.setHorizontalGroup(
            cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cbOrganizedMarketPlaceLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cbOrganizedMarketPlaceLayout.createSequentialGroup()
                        .addGroup(cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(cbOrganizedMarketPlaceLayout.createSequentialGroup()
                                .addGroup(cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(cbOrganizedMarketPlaceLayout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addGap(18, 18, 18)
                                        .addComponent(tfContractId, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(cbOrganizedMarketPlaceLayout.createSequentialGroup()
                                        .addComponent(jLabel11)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(cbSettlementMethod, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(9, 9, 9)
                                .addGroup(cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel8))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cbOrganisedMarketPlace, 0, 87, Short.MAX_VALUE)
                                    .addComponent(cbContractName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(cbOrganizedMarketPlaceLayout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jcalStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 89, Short.MAX_VALUE)
                                .addComponent(jLabel15)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(cbOrganizedMarketPlaceLayout.createSequentialGroup()
                                .addGroup(cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(cbOrganizedMarketPlaceLayout.createSequentialGroup()
                                        .addComponent(jLabel9)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(cbContractType, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jCalEnddate, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                                .addGroup(cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cbOrganizedMarketPlaceLayout.createSequentialGroup()
                                        .addComponent(jLabel10)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cbEnergyCommodity, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cbOrganizedMarketPlaceLayout.createSequentialGroup()
                                        .addComponent(jLabel16)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cbLoadType, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(29, 29, 29))
                            .addGroup(cbOrganizedMarketPlaceLayout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbDeliveryPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(cbOrganizedMarketPlaceLayout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        cbOrganizedMarketPlaceLayout.setVerticalGroup(
            cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cbOrganizedMarketPlaceLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(tfContractId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(cbContractType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(cbEnergyCommodity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbContractName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(cbSettlementMethod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(cbOrganisedMarketPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(cbDeliveryPoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cbOrganizedMarketPlaceLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel15)
                            .addGroup(cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel16)
                                .addComponent(cbLoadType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel14))
                        .addGap(18, 18, 18))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cbOrganizedMarketPlaceLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(cbOrganizedMarketPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jCalEnddate, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jcalStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)))
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jLabel20.setText("Transaction Time:");

        jLabel21.setText("Unique Transaction Identifier:");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Price Details"));

        jLabel23.setText("Price:");

        jLabel24.setText("Price currency:");

        cbCurrency.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbCurrency.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbCurrencyItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addGap(57, 57, 57)
                        .addComponent(tfPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel24)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbCurrency, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(15, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(tfPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(cbCurrency, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Notional Amount Details"));

        jLabel25.setText("Notional Amount:");

        jLabel26.setText("Notional Currency:");

        tfNotionalAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfNotionalAmountActionPerformed(evt);
            }
        });
        tfNotionalAmount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfNotionalAmountKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tfNotionalAmount))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel26)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfNotionalCurrency, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(tfNotionalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(tfNotionalCurrency, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel22.setText("Action Type:");

        cbActionType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Quantity"));

        jLabel27.setText("Value:");

        jLabel28.setText("Unit:");

        cbQuantityUnit.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel27)
                    .addComponent(jLabel28))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cbQuantityUnit, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tfQuantityValue))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(tfQuantityValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(cbQuantityUnit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder("Total Notional Contract Quantity"));

        tfTotalNValue.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfTotalNValueFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfTotalNValueFocusLost(evt);
            }
        });
        tfTotalNValue.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                tfTotalNValueInputMethodTextChanged(evt);
            }
        });
        tfTotalNValue.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tfTotalNValuePropertyChange(evt);
            }
        });

        jLabel29.setText("Value:");

        cbTotNotionalUnit.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel30.setText("Unit:");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel29)
                    .addComponent(jLabel30))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(tfTotalNValue)
                        .addGap(130, 130, 130))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(cbTotNotionalUnit, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(123, 123, 123))))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(tfTotalNValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(cbTotNotionalUnit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonGenerateXML.setText("Generate XML");
        buttonGenerateXML.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonGenerateXMLActionPerformed(evt);
            }
        });

        jLabel31.setText("Trader id for Market Participant:");

        jLabel32.setText("Organised Market Place ID:");

        cbOrganisedMarketPlace1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        butUpdate.setText("Update changes");
        butUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butUpdateActionPerformed(evt);
            }
        });

        cbAcerCodes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel3.setText("Linked Transaction Id:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tfTransactionTime, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel21)
                        .addGap(18, 18, 18)
                        .addComponent(tfUniqueTransId, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbActionType, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4))
                        .addGap(23, 23, 23)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfRecordNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfBeneficiaryId, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbMarketParticipant, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbTradingCapacity, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbBuysellIndicator, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(cbAcerCodes, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(tfOtherMarketParticipant, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 85, Short.MAX_VALUE)
                                        .addComponent(jLabel32)
                                        .addGap(27, 27, 27))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel31)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(tfTraderForMarketParticipant, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addComponent(cbOrganisedMarketPlace1, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(47, 47, 47))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbOrganizedMarketPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(35, 73, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(buttonGenerateXML, javax.swing.GroupLayout.PREFERRED_SIZE, 318, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(152, 152, 152)
                        .addComponent(butUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfLinkedTransactionId))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tfRecordNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31)
                    .addComponent(tfTraderForMarketParticipant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel32)
                        .addComponent(cbOrganisedMarketPlace1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(cbMarketParticipant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tfOtherMarketParticipant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cbAcerCodes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(tfBeneficiaryId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(cbTradingCapacity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(cbBuysellIndicator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbOrganizedMarketPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(tfTransactionTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(tfUniqueTransId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22)
                    .addComponent(cbActionType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(butUpdate)
                            .addComponent(buttonGenerateXML)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(tfLinkedTransactionId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 34, Short.MAX_VALUE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cbLoadTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbLoadTypeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbLoadTypeActionPerformed

    private void tfTotalNValueInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_tfTotalNValueInputMethodTextChanged

        // TODO add your handling code here:
    }//GEN-LAST:event_tfTotalNValueInputMethodTextChanged

    private void tfTotalNValuePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tfTotalNValuePropertyChange

//           int value = Integer.parseInt(tfTotalNValue.getText());
//         double price = Double.parseDouble(tfPrice.getText());
//         int suma= (int) (value*price);
//        tfNotionalAmount.setText(""+suma);
        // TODO add your handling code here:
    }//GEN-LAST:event_tfTotalNValuePropertyChange

    private void tfTotalNValueFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfTotalNValueFocusGained
        // TODO add your handling code here:          
//        int sum = 0;
//
//        try {
//            if (!tfTotalNValue.getText().isEmpty() && !tfPrice.getText().isEmpty()) {
//                int value = Integer.parseInt(tfTotalNValue.getText());//we must add this
//
//                double price = Double.parseDouble(tfPrice.getText());//we must add this
//
//                sum = (int) (value * price);
//            }
//
//            tfNotionalAmount.setText(String.valueOf(sum));
//
//        } catch (NumberFormatException ex) {
//            JOptionPane.showMessageDialog(this, "Price i total value moraju biti brojevi!", "Greska pri unosu!", JOptionPane.ERROR_MESSAGE);
//
//        }
    }//GEN-LAST:event_tfTotalNValueFocusGained

    private void tfNotionalAmountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfNotionalAmountKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_tfNotionalAmountKeyTyped

    private void cbCurrencyItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbCurrencyItemStateChanged
        // TODO add your handling code here:

        String currency = cbCurrency.getSelectedItem().toString();
        tfNotionalCurrency.setText(currency);


    }//GEN-LAST:event_cbCurrencyItemStateChanged

    private void buttonGenerateXMLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonGenerateXMLActionPerformed
        try {
            // TODO add your handling code here:
            ObjectFactory factory = new ObjectFactory();
            ReportingEntityID entityID = factory.createReportingEntityID();
            entityID.setAce("B0000450I.HU");

            AnnexTable1Trade annexTable = factory.createAnnexTable1Trade();
            annexTable.setRecordSeqNumber(new BigInteger(tfRecordNumber.getText()));
            ParticipantType type = factory.createParticipantType();

            //Id of market participant LE TRADING
            type.setAce("A00036875.SK");
            annexTable.setIdOfMarketParticipant(type);

            TraderIDType traderIdType = factory.createTraderIDType();
            if (!tfTraderForMarketParticipant.getText().isEmpty()) {
                traderIdType.setTraderIdForMarketParticipant(tfTraderForMarketParticipant.getText().trim());
                annexTable.setTraderID(traderIdType);
            }

            ParticipantType participantType = factory.createParticipantType();
            String otherType = cbMarketParticipant.getSelectedItem().toString();

            if (otherType.equals("ace")) {
                AccerCodes acerCodes = (AccerCodes)cbAcerCodes.getSelectedItem();
                participantType.setAce(acerCodes.getAccerCode());
//                int acerCode = cbAcerCodes.getSelectedIndex();
//                switch (acerCode) {
//                    case 0:
//                        participantType.setAce(AccerCodes.ELPETRA_BG);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        System.out.println(acerCode);
//                        break;
//                    case 1:
//                        participantType.setAce(AccerCodes.KOZLODUY);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        System.out.println(acerCode);
//                        break;
//                    case 2:
//                        participantType.setAce(AccerCodes.ELPETRA_CY);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 3:
//                        participantType.setAce(AccerCodes.PPC);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 4:
//                        participantType.setAce(AccerCodes.HERON);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 5:
//                        participantType.setAce(AccerCodes.ELPEDISON);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 6:
//                        participantType.setAce(AccerCodes.PROTERGIA);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 7:
//                        participantType.setAce(AccerCodes.EDF);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 8:
//                        participantType.setAce(AccerCodes.EUROPE_ENERGY);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 9:
//                        participantType.setAce(AccerCodes.EDS_RO);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 10:
//                        participantType.setAce(AccerCodes.AYEN);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 11:
//                        participantType.setAce(AccerCodes.EDS_SK);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 12:
//                        participantType.setAce(AccerCodes.SEE_POWER);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 13:
//                        participantType.setAce(AccerCodes.TPP_MARITSA);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 14:
//                        participantType.setAce(AccerCodes.TRANSENERGO);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 15:
//                        participantType.setAce(AccerCodes.NEAS_ENERGY_DK);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 16:
//                        participantType.setAce(AccerCodes.PETROL_SI);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 17:
//                        participantType.setAce(AccerCodes.ENERGANA_CZ);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 18:
//                        participantType.setAce(AccerCodes.ENERGO_PRO_BG);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                    case 19:
//                        participantType.setAce(AccerCodes.EDS_HR);
//                        Kontroler.getInstance().setCompany(acerCodes);
//                        break;
//                }
//                    if (tfOtherMarketParticipant.getText().length() == 12) {
//                        participantType.setAce(tfOtherMarketParticipant.getText().trim());
//                    } else {
//                        throw new Exception("Acer code za other market participant mora da ima 12 karaktera!");
//                    }

            } else if (otherType.equals("bic")) {
                if (tfOtherMarketParticipant.getText().trim().length() == 11) {
                    participantType.setBic(tfOtherMarketParticipant.getText().trim());
                } else {
                    throw new Exception("Bic code mora da ima 11 karaktera!");
                }
            } else if (otherType.equals("eic")) {
                if (tfOtherMarketParticipant.getText().trim().length() == 16) {
                    participantType.setEic(tfOtherMarketParticipant.getText().trim());
                } else {
                    throw new Exception("eic code mora da ima 16 karaktera!");
                }
            } else if (otherType.equals("gln")) {
                if (tfOtherMarketParticipant.getText().trim().length() == 13) {
                    participantType.setGln(tfOtherMarketParticipant.getText().trim());
                } else {
                    throw new Exception("GLN code mora da ima 13 karaktera!");
                }
            } else if (tfOtherMarketParticipant.getText().trim().length() == 20) {
                participantType.setLei(tfOtherMarketParticipant.getText().trim());
            } else {
                throw new Exception("LEI code mora da ima 13 karaktera!");
            }
            annexTable.setOtherMarketParticipant(participantType);

            ParticipantType type2 = factory.createParticipantType();
            if (!tfBeneficiaryId.getText().isEmpty()) {
                if (tfBeneficiaryId.getText().trim().length() == 12) {
                    type2.setAce(tfBeneficiaryId.getText().trim());
                    annexTable.setBeneficiaryIdentification(type2);
                } else {
                    throw new Exception("Acer code za Beneficiary Identification mora da ima 12 karaktera!");
                }
            }

            String tradingItem = cbTradingCapacity.getSelectedItem().toString();
            if (tradingItem.toLowerCase().equals("p")) {
                annexTable.setTradingCapacity(TradingCapacityType.P);
            } else {
                annexTable.setTradingCapacity(TradingCapacityType.A);
            }

            String sellingInd = cbBuysellIndicator.getSelectedItem().toString();
            if (sellingInd.toUpperCase().equals("B")) {
                annexTable.setBuySellIndicator(BuySellIndicatorType.B);
            } else if (sellingInd.toUpperCase().equals("C")) {
                annexTable.setBuySellIndicator(BuySellIndicatorType.C);
            } else {
                annexTable.setBuySellIndicator(BuySellIndicatorType.S);
            }

            AnnexTable1ContractType contractInfo = factory.createAnnexTable1ContractType();
            if (!tfContractId.getText().isEmpty()) {
                contractInfo.setContractId(tfContractId.getText().trim());
            }
            contractInfo.setContractName(cbContractName.getSelectedItem().toString());

            String energyCommodityType = cbEnergyCommodity.getSelectedItem().toString();
            if (energyCommodityType.equals("EL")) {
                contractInfo.getEnergyCommodity().add(EnergyCommodityType.EL);
            } else {
                contractInfo.getEnergyCommodity().add(EnergyCommodityType.NG);
            }
            String contractType = cbContractType.getSelectedItem().toString();
            if (contractType.equals("AU")) {
                contractInfo.setContractType(ContractTypeType.AU);
            } else if (contractType.equals("CO")) {
                contractInfo.setContractType(ContractTypeType.CO);
            } else if (contractType.equals("FW")) {
                contractInfo.setContractType(ContractTypeType.FW);
            } else if (contractType.equals("OP")) {
                contractInfo.setContractType(ContractTypeType.OP);
            } else if (contractType.equals("OP_FW")) {
                contractInfo.setContractType(ContractTypeType.OP_FW);
            } else if (contractType.equals("OP_FU")) {
                contractInfo.setContractType(ContractTypeType.OP_FU);
            } else if (contractType.equals("OP_SW")) {
                contractInfo.setContractType(ContractTypeType.OP_SW);
            } else if (contractType.equals("SP")) {
                contractInfo.setContractType(ContractTypeType.SP);
            } else if (contractType.equals("SW")) {
                contractInfo.setContractType(ContractTypeType.SW);
            } else if (contractType.equals("OT")) {
                contractInfo.setContractType(ContractTypeType.OT);
            } else {
                contractInfo.setContractType(ContractTypeType.FU);
            }

            String settlementItem = cbSettlementMethod.getSelectedItem().toString();
            if (settlementItem.toUpperCase().equals("P")) {
                contractInfo.setSettlementMethod(SettlementMethodType.P);
            } else if (settlementItem.toUpperCase().equals("C")) {
                contractInfo.setSettlementMethod(SettlementMethodType.C);
            } else {
                contractInfo.setSettlementMethod(SettlementMethodType.O);
            }

            OrganisedMarketPlaceType marketPlaceType = factory.createOrganisedMarketPlaceType();
            OrganisedMarketPlaceType marketPlaceType1 = factory.createOrganisedMarketPlaceType();
            String marketPlaceItem = cbOrganisedMarketPlace.getSelectedItem().toString();

            if (marketPlaceItem.toLowerCase().equals("lei")) {
                marketPlaceType.setLei("LEI");
            } else if (marketPlaceItem.toLowerCase().equals("mic")) {
                marketPlaceType.setMic("MIC");
            } else if (marketPlaceItem.toLowerCase().equals("acer")) {
                marketPlaceType.setAce("ACER");
            } else {
                marketPlaceType.setBil("XBIL");
            }
            contractInfo.setOrganisedMarketPlaceIdentifier(marketPlaceType);

            DeliveryPoint deliveryPointZone = (DeliveryPoint)cbDeliveryPoint.getSelectedItem();
            contractInfo.getDeliveryPointOrZone().add(deliveryPointZone.getCodeDelivery());
//            int delivery = cbDeliveryPoint.getSelectedIndex();
//            switch (delivery) {
//                case 0:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HUNGARY);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 1:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.BULGARIA);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 2:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.SLOVAKIA);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 3:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.IT_SL);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 4:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.GR_IT);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 5:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.GR_TR);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 6:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.GR_MK);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 7:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HR_SL);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 8:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HR_HU);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 9:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HR_BA);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 10:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HR_RS);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 11:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HU_RO);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 12:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HU_SK);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 13:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HU_UA);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 14:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HU_AT);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 15:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HU_RS);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 16:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.BA_RS);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 17:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.ME_BA);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                case 18:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.ME_RS);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 19:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.ME_AL);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//
//                case 20:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.AL_GR);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 21:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.AL_RS);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//
//                case 22:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.BG_GR);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 23:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.BG_TR);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 24:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.BG_RS);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 25:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.MK_RS);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 26:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.RO_RS);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 27:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.RO_UA);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 28:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.RO_BG);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 29:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.CZ_SK);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 30:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.AT_CZ);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 31:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.AT_SL);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//                case 32:
//                    contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.APG_TENNET);
//                    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                    System.out.println(deliveryPointZone);
//                    break;
//            }
//            if (!tfdeliveryPoint.getText().isEmpty()) {
//                String deliveryPoint = tfdeliveryPoint.getText().trim();
//                contractInfo.getDeliveryPointOrZone().add(deliveryPoint);
//            }

            String startDate = ((JTextField) jcalStartDate.getDateEditor().getUiComponent()).getText();
            System.out.println(startDate);
            String endDate = ((JTextField) jCalEnddate.getDateEditor().getUiComponent()).getText();

            if (!startDate.isEmpty()) {
                XMLGregorianCalendar xmlDate2 = stringToXMLGregorianCalendar(startDate);
                contractInfo.setDeliveryStartDate(xmlDate2);
            } else {
                throw new Exception("Morate da izaberete start date");
                //JOptionPane.showMessageDialog(this, "Morate da izaberete start date", "Greska!", JOptionPane.ERROR_MESSAGE);
            }

            if (!endDate.isEmpty()) {

                XMLGregorianCalendar xmlDate3 = stringToXMLGregorianCalendar(endDate);
                contractInfo.setDeliveryEndDate(xmlDate3);
            } else {
                throw new Exception("Morate da izaberete end date");
            }

            String loadItem = cbLoadType.getSelectedItem().toString();
            if (loadItem.toUpperCase().equals("BL")) {
                contractInfo.setLoadType(ContractLoadType.BL);
            } else if (loadItem.toUpperCase().equals("PL")) {
                contractInfo.setLoadType(ContractLoadType.PL);
            } else if (loadItem.toUpperCase().equals("OP")) {
                contractInfo.setLoadType(ContractLoadType.OP);
            } else if (loadItem.toUpperCase().equals("BH")) {
                contractInfo.setLoadType(ContractLoadType.BH);
            } else if (loadItem.toUpperCase().equals("SH")) {
                contractInfo.setLoadType(ContractLoadType.SH);
            } else if (loadItem.toUpperCase().equals("GD")) {
                contractInfo.setLoadType(ContractLoadType.GD);
            } else {
                contractInfo.setLoadType(ContractLoadType.OT);
            }

            DeliveryProfileDetails deliveryProfile = factory.createDeliveryProfileDetails();

            XMLGregorianCalendar startDate1 = stringToXMLGregorianCalendarZOne(tfStartTime.getText().trim());
            XMLGregorianCalendar end2 = stringToXMLGregorianCalendarZOne(tfEndTime.getText().trim());
            JAXBElement<XMLGregorianCalendar> element = factory.createDeliveryProfileDetailsLoadDeliveryStartTime(startDate1);
            JAXBElement<XMLGregorianCalendar> element2 = factory.createDeliveryProfileDetailsLoadDeliveryEndTime(end2);

            deliveryProfile.getLoadDeliveryStartTimeAndLoadDeliveryEndTime().add(element);
            deliveryProfile.getLoadDeliveryStartTimeAndLoadDeliveryEndTime().add(element2);
            if (!cbDaysOfWeek.getSelectedItem().toString().equals("")) {
                deliveryProfile.getDaysOfTheWeek().add(cbDaysOfWeek.getSelectedItem().toString());
            }
            contractInfo.getDeliveryProfile().add(deliveryProfile);

            // if(!tfTransactionTime.getText().isEmpty()){
            XMLGregorianCalendar transactionTime = stringToXMLTransactionDate(tfTransactionTime.getText());
            annexTable.setTransactionTime(transactionTime);

            TradeIdType tradeId = factory.createTradeIdType();
            if (!tfUniqueTransId.getText().isEmpty()) {
                tradeId.setUniqueTransactionIdentifier(tfUniqueTransId.getText().trim());
                annexTable.setUniqueTransactionIdentifier(tradeId);
            }
            
            String linkedTransactionId=tfLinkedTransactionId.getText().trim();
            if(!linkedTransactionId.isEmpty()){
            annexTable.getLinkedTransactionId().add(linkedTransactionId);
            }

            String marketPlaceItem1 = cbOrganisedMarketPlace1.getSelectedItem().toString();

            if (marketPlaceItem1.toLowerCase().equals("lei")) {
                marketPlaceType1.setLei("LEI");
            } else if (marketPlaceItem1.toLowerCase().equals("mic")) {
                marketPlaceType1.setMic("MIC");
            } else if (marketPlaceItem1.toLowerCase().equals("acer")) {
                marketPlaceType1.setAce("ACER");
            } else {
                marketPlaceType1.setBil("XBIL");
            }

            annexTable.setOrganisedMarketPlaceIdentifier(marketPlaceType1);

            String actionType = cbActionType.getSelectedItem().toString();
            if (actionType.toLowerCase().equals("e")) {
                annexTable.setActionType(ActionTypesType.E);
            } else if (actionType.toLowerCase().equals("c")) {
                annexTable.setActionType(ActionTypesType.C);
            } else if (actionType.toLowerCase().equals("m")) {
                annexTable.setActionType(ActionTypesType.M);
            } else {
                annexTable.setActionType(ActionTypesType.N);
            }

            PriceDetailsType priceType = factory.createPriceDetailsType();
            priceType.setPrice(new BigDecimal(tfPrice.getText()));

            String priceCurrencyType = cbCurrency.getSelectedItem().toString();
            if (priceCurrencyType.toUpperCase().equals("EUR")) {
                priceType.setPriceCurrency(CurrencyCodeType.EUR);
            } else if (priceCurrencyType.toUpperCase().equals("BGN")) {
                priceType.setPriceCurrency(CurrencyCodeType.BGN);
            } else if (priceCurrencyType.toUpperCase().equals("CHF")) {
                priceType.setPriceCurrency(CurrencyCodeType.CHF);
            } else if (priceCurrencyType.toUpperCase().equals("CZK")) {
                priceType.setPriceCurrency(CurrencyCodeType.CZK);
            } else if (priceCurrencyType.toUpperCase().equals("DKK")) {
                priceType.setPriceCurrency(CurrencyCodeType.DKK);
            } else if (priceCurrencyType.toUpperCase().equals("EUX")) {
                priceType.setPriceCurrency(CurrencyCodeType.EUX);
            } else if (priceCurrencyType.toUpperCase().equals("GBP")) {
                priceType.setPriceCurrency(CurrencyCodeType.GBP);
            } else if (priceCurrencyType.toUpperCase().equals("GBX")) {
                priceType.setPriceCurrency(CurrencyCodeType.GBX);
            } else if (priceCurrencyType.toUpperCase().equals("HRK")) {
                priceType.setPriceCurrency(CurrencyCodeType.HRK);
            } else if (priceCurrencyType.toUpperCase().equals("HUF")) {
                priceType.setPriceCurrency(CurrencyCodeType.HUF);
            } else if (priceCurrencyType.toUpperCase().equals("ISK")) {
                priceType.setPriceCurrency(CurrencyCodeType.ISK);
            } else if (priceCurrencyType.toUpperCase().equals("NOK")) {
                priceType.setPriceCurrency(CurrencyCodeType.NOK);
            } else if (priceCurrencyType.toUpperCase().equals("PCT")) {
                priceType.setPriceCurrency(CurrencyCodeType.PCT);
            } else if (priceCurrencyType.toUpperCase().equals("PLN")) {
                priceType.setPriceCurrency(CurrencyCodeType.PLN);
            } else if (priceCurrencyType.toUpperCase().equals("RON")) {
                priceType.setPriceCurrency(CurrencyCodeType.RON);
            } else if (priceCurrencyType.toUpperCase().equals("SEK")) {
                priceType.setPriceCurrency(CurrencyCodeType.SEK);
            } else {
                priceType.setPriceCurrency(CurrencyCodeType.USD);
            }

            annexTable.setPriceDetails(priceType);

            NotionalAmountDetailsType notionalDetailsType = factory.createNotionalAmountDetailsType();
            notionalDetailsType.setNotionalAmount(new BigDecimal(tfNotionalAmount.getText()));

            String notionalCurrency = tfNotionalCurrency.getText();
            if (notionalCurrency.toUpperCase().equals("EUR")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.EUR);
            } else if (notionalCurrency.toUpperCase().equals("BGN")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.BGN);
            } else if (notionalCurrency.toUpperCase().equals("CHF")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.CHF);
            } else if (notionalCurrency.toUpperCase().equals("CZK")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.CZK);
            } else if (notionalCurrency.toUpperCase().equals("DKK")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.DKK);
            } else if (notionalCurrency.toUpperCase().equals("EUX")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.EUX);
            } else if (notionalCurrency.toUpperCase().equals("GBP")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.GBP);
            } else if (notionalCurrency.toUpperCase().equals("GBX")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.GBX);
            } else if (notionalCurrency.toUpperCase().equals("HRK")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.HRK);
            } else if (notionalCurrency.toUpperCase().equals("HUF")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.HUF);
            } else if (notionalCurrency.toUpperCase().equals("ISK")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.ISK);
            } else if (notionalCurrency.toUpperCase().equals("NOK")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.NOK);
            } else if (notionalCurrency.toUpperCase().equals("PCT")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.PCT);
            } else if (notionalCurrency.toUpperCase().equals("PLN")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.PLN);
            } else if (notionalCurrency.toUpperCase().equals("RON")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.RON);
            } else if (notionalCurrency.toUpperCase().equals("SEK")) {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.SEK);
            } else {
                notionalDetailsType.setNotionalCurrency(CurrencyCodeType.USD);
            }
            annexTable.setNotionalAmountDetails(notionalDetailsType);

            QuantityType quantityType = factory.createQuantityType();
            quantityType.setValue(new BigDecimal(tfQuantityValue.getText()));

            String quantityUnit = cbQuantityUnit.getSelectedItem().toString();
            if (quantityUnit.equals("KW")) {
                quantityType.setUnit(QuantityUnitType.KW);
            } else if (quantityUnit.equals("MW")) {
                quantityType.setUnit(QuantityUnitType.MW);
            } else if (quantityUnit.equals("cm/d")) {
                quantityType.setUnit(QuantityUnitType.CM_D);
            } else if (quantityUnit.equals("GW")) {
                quantityType.setUnit(QuantityUnitType.GW);
            } else if (quantityUnit.equals("GWh/d")) {
                quantityType.setUnit(QuantityUnitType.G_WH_D);
            } else if (quantityUnit.equals("GWh/h")) {
                quantityType.setUnit(QuantityUnitType.G_WH_H);
            } else if (quantityUnit.equals("KTherm/d")) {
                quantityType.setUnit(QuantityUnitType.K_THERM_D);
            } else if (quantityUnit.equals("KWh/d")) {
                quantityType.setUnit(QuantityUnitType.K_WH_D);
            } else if (quantityUnit.equals("KWh/h")) {
                quantityType.setUnit(QuantityUnitType.K_WH_H);
            } else if (quantityUnit.equals("mcm/d")) {
                quantityType.setUnit(QuantityUnitType.MCM_D);
            } else if (quantityUnit.equals("MTherm/d")) {
                quantityType.setUnit(QuantityUnitType.M_THERM_D);
            } else if (quantityUnit.equals("MWh/d")) {
                quantityType.setUnit(QuantityUnitType.M_WH_D);
            } else if (quantityUnit.equals("MWh/h")) {
                quantityType.setUnit(QuantityUnitType.M_WH_H);
            } else {
                quantityType.setUnit(QuantityUnitType.THERM_D);
            }
            annexTable.setQuantity(quantityType);

            NotionalQuantityType totalNotionalType = factory.createNotionalQuantityType();
            totalNotionalType.setValue(new BigDecimal(tfTotalNValue.getText()));

            String tquantityUnit = cbTotNotionalUnit.getSelectedItem().toString();
            if (tquantityUnit.equals("cm")) {
                totalNotionalType.setUnit(NotionalQuantityUnitType.CM);
            } else if (tquantityUnit.equals("GWh")) {
                totalNotionalType.setUnit(NotionalQuantityUnitType.G_WH);
            } else if (tquantityUnit.equals("KTherm")) {
                totalNotionalType.setUnit(NotionalQuantityUnitType.K_THERM);
            } else if (tquantityUnit.equals("KWh")) {
                totalNotionalType.setUnit(NotionalQuantityUnitType.K_WH);
            } else if (tquantityUnit.equals("mcm")) {
                totalNotionalType.setUnit(NotionalQuantityUnitType.MCM);
            } else if (tquantityUnit.equals("MTherm")) {
                totalNotionalType.setUnit(NotionalQuantityUnitType.M_THERM);
            } else if (tquantityUnit.equals("Therm")) {
                totalNotionalType.setUnit(NotionalQuantityUnitType.THERM);
            } else {
                totalNotionalType.setUnit(NotionalQuantityUnitType.M_WH);
            }
            annexTable.setTotalNotionalContractQuantity(totalNotionalType);

            ContractInfoType1 contractInfoType1 = factory.createContractInfoType1();
            contractInfoType1.setContract(contractInfo);
            annexTable.setContractInfo(contractInfoType1);

            TradeListType tradeList = factory.createTradeListType();
            tradeList.getTradeReport().add(annexTable);

            Kontroler.getInstance().sacuvajReportUTabeli(annexTable);

            REMITTable1 table1 = factory.createREMITTable1();
            table1.setReportingEntityID(entityID);
            table1.setTradeList(tradeList);

            JAXBContext context = JAXBContext.newInstance(table1.getClass().getPackage().getName());
            //    JAXBElement <REMITTable1> element=factory.createREMITTable1(table1);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
            String userHomeFolder = System.getProperty("user.home");

            File textFile = new File(userHomeFolder + "/Desktop", "RemitTable1.xml");
            // File file = new File("Desktop/RemitTable1.xml");
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.acer.europa.eu/REMIT/REMITTable1_V1.xsd");
            marshaller.marshal(table1, System.out);
            marshaller.marshal(table1, textFile);

            if (isTableForAdded) {
                JOptionPane.showMessageDialog(this, "Your report is added on ReportTable! ", "Message", 1);
            } else {
                JOptionPane.showMessageDialog(this, "Your RemitTable1.xml is saved on Desktop! ", "Message", 1);
            }

            // setRbr();
            Window w = SwingUtilities.getWindowAncestor(PanelRemitTable1.this);
            w.setVisible(false);

        } catch (JAXBException ex) {
            Logger.getLogger(PanelRemitTable1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(PanelRemitTable1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Greška", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(PanelRemitTable1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this, "U polja Price, Value i Record Number moraju da se unesu brojevi", "Greska!", JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Greška", JOptionPane.ERROR_MESSAGE);
        }


    }//GEN-LAST:event_buttonGenerateXMLActionPerformed

    private void tfTotalNValueFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfTotalNValueFocusLost
        // TODO add your handling code here:
        int sum = 0;
        double sumD = 0;
        int type = 0;

        try {
            if (!tfTotalNValue.getText().isEmpty() && !tfPrice.getText().isEmpty()) {

                int value = Integer.parseInt(tfTotalNValue.getText());//we must add this
                int price = Integer.parseInt(tfPrice.getText().trim());//we must add
                sum = value * price;
                System.out.println("suma" + sum);

            }

            tfNotionalAmount.setText(String.valueOf(sum));

        } catch (NumberFormatException ex) {
            try {
                double price = Double.parseDouble(tfPrice.getText().trim());
                int value = 0;
                double val = 0;
                try {
                    value = Integer.parseInt(tfTotalNValue.getText());
                } catch (NumberFormatException ex2) {
                    val = Double.parseDouble(tfTotalNValue.getText());
                    double sumd = price * val;
                    DecimalFormat df = new DecimalFormat("0.00");
                    String formate = df.format(sumd);
                    double finalValue = (double) df.parse(formate);
                    tfNotionalAmount.setText(String.valueOf(finalValue));
                }
                double sumd = price * value;
                DecimalFormat df = new DecimalFormat("0.00");
                String formate = df.format(sumd);
                double finalValue = (double) df.parse(formate);
                tfNotionalAmount.setText(String.valueOf(finalValue));
                //   ex.printStackTrace();
                //   JOptionPane.showMessageDialog(this, "Price i total value moraju biti brojevi!", "Greska pri unosu!", JOptionPane.ERROR_MESSAGE);
            } catch (ParseException ex1) {
                Logger.getLogger(PanelRemitTable1.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

//        } catch (ParseException ex) {
//            Logger.getLogger(PanelRemitTable1.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }//GEN-LAST:event_tfTotalNValueFocusLost

    private void butUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butUpdateActionPerformed
        // TODO add your handling code here:
        try {

            int odgovor = JOptionPane.showConfirmDialog(this, "Da li ste sigurni da zelite da izmenite podatke?", "Update changes", JOptionPane.YES_NO_OPTION);
            if (odgovor == JOptionPane.OK_OPTION) {
                ObjectFactory factory = new ObjectFactory();
                AnnexTable1Trade table = Kontroler.getInstance().vratiTradeListuSaRecNumber(tfRecordNumber.getText());
                ParticipantType type = factory.createParticipantType();
//                if (!tfIdOfMarketParticipant.getText().trim().isEmpty()) {
//                    if (tfIdOfMarketParticipant.getText().length() != 12) {
//
//                        throw new Exception("Acer code mora da ima 12 karaktera!");
//
//                    } else {
//                        type.setAce(tfIdOfMarketParticipant.getText().trim());
//
//                        table.setIdOfMarketParticipant(type);
//                    }
//
//                } else {
//                    //  JOptionPane.showMessageDialog(this, "Polje IdOfMarketParticipant ne sme biti prazno", "Greska!", JOptionPane.ERROR_MESSAGE);
//                    throw new Exception("Morate da unesete Id Of Market Participant!");
//                }

                TraderIDType traderIdType = factory.createTraderIDType();
                if (!tfTraderForMarketParticipant.getText().isEmpty()) {
                    traderIdType.setTraderIdForMarketParticipant(tfTraderForMarketParticipant.getText().trim());
                    table.setTraderID(traderIdType);
                } else {
                    table.setTraderID(null);
                }

                ParticipantType participantType = factory.createParticipantType();
                String otherType = cbMarketParticipant.getSelectedItem().toString();

                if (otherType.equals("ace")) {
                    AccerCodes acerCodes = (AccerCodes)cbAcerCodes.getSelectedItem();
                     participantType.setAce(acerCodes.getAccerCode());
//                    int acerCode = cbAcerCodes.getSelectedIndex();
//                    switch (acerCode) {
//                        case 0:
//                            participantType.setAce(AccerCodes.ELPETRA_BG);
//                            // Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 1:
//                            participantType.setAce(AccerCodes.KOZLODUY);
//                            //   Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 2:
//                            participantType.setAce(AccerCodes.ELPETRA_CY);
//                            //    Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 3:
//                            participantType.setAce(AccerCodes.PPC);
//                            //     Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 4:
//                            participantType.setAce(AccerCodes.HERON);
//                            //    Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 5:
//                            participantType.setAce(AccerCodes.ELPEDISON);
//                            //      Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 6:
//                            participantType.setAce(AccerCodes.PROTERGIA);
//                            //     Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 7:
//                            participantType.setAce(AccerCodes.EDF);
//                            //    Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 8:
//                            participantType.setAce(AccerCodes.EUROPE_ENERGY);
//                            //     Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 9:
//                            participantType.setAce(AccerCodes.EDS_RO);
//                            //      Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 10:
//                            participantType.setAce(AccerCodes.AYEN);
//                            //    Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 11:
//                            participantType.setAce(AccerCodes.EDS_SK);
//                            //      Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 12:
//                            participantType.setAce(AccerCodes.SEE_POWER);
//                            //     Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 13:
//                            participantType.setAce(AccerCodes.TPP_MARITSA);
//                            //     Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 14:
//                            participantType.setAce(AccerCodes.TRANSENERGO);
//                            //       Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//
//                        case 15:
//                            participantType.setAce(AccerCodes.NEAS_ENERGY_DK);
//                            //       Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//
//                        case 16:
//                            participantType.setAce(AccerCodes.PETROL_SI);
//                            //       Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 17:
//                            participantType.setAce(AccerCodes.ENERGANA_CZ);
//                            //       Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 18:
//                            participantType.setAce(AccerCodes.ENERGO_PRO_BG);
//                            //       Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                        case 19:
//                            participantType.setAce(AccerCodes.EDS_HR);
//                            //       Kontroler.getInstance().setCompany(acerCodes);
//                            break;
//                    }
//                    if (tfOtherMarketParticipant.getText().length() == 12) {
//                        participantType.setAce(tfOtherMarketParticipant.getText().trim());
//                    } else {
//                        throw new Exception("Acer code za other market participant mora da ima 12 karaktera!");
//                    }

                } else if (otherType.equals("bic")) {
                    if (tfOtherMarketParticipant.getText().trim().length() == 11) {
                        participantType.setBic(tfOtherMarketParticipant.getText().trim());
                    } else {
                        throw new Exception("Bic code mora da ima 11 karaktera!");
                    }
                } else if (otherType.equals("eic")) {
                    if (tfOtherMarketParticipant.getText().trim().length() == 16) {
                        participantType.setEic(tfOtherMarketParticipant.getText().trim());
                    } else {
                        throw new Exception("eic code mora da ima 16 karaktera!");
                    }
                } else if (otherType.equals("gln")) {
                    if (tfOtherMarketParticipant.getText().trim().length() == 13) {
                        participantType.setGln(tfOtherMarketParticipant.getText().trim());
                    } else {
                        throw new Exception("GLN code mora da ima 13 karaktera!");
                    }
                } else if (tfOtherMarketParticipant.getText().trim().length() == 20) {
                    participantType.setLei(tfOtherMarketParticipant.getText().trim());
                } else {
                    throw new Exception("LEI code mora da ima 13 karaktera!");
                }
                table.setOtherMarketParticipant(participantType);

                ParticipantType type2 = factory.createParticipantType();
                if (!tfBeneficiaryId.getText().isEmpty()) {
                    if (tfBeneficiaryId.getText().length() == 12) {
                        type2.setAce(tfBeneficiaryId.getText().trim());
                        table.setBeneficiaryIdentification(type2);
                    } else {
                        throw new Exception("Acer code za BeneficiaryIdentification mora da ima 12 karaktera!");
                    }
                } else {
                    table.setBeneficiaryIdentification(null);
                }

                String tradingItem = cbTradingCapacity.getSelectedItem().toString();
                if (tradingItem.toLowerCase().equals("p")) {
                    table.setTradingCapacity(TradingCapacityType.P);
                } else {
                    table.setTradingCapacity(TradingCapacityType.A);
                }

                String sellingInd = cbBuysellIndicator.getSelectedItem().toString();
                if (sellingInd.toUpperCase().equals("B")) {
                    table.setBuySellIndicator(BuySellIndicatorType.B);
                } else if (sellingInd.toUpperCase().equals("C")) {
                    table.setBuySellIndicator(BuySellIndicatorType.C);
                } else {
                    table.setBuySellIndicator(BuySellIndicatorType.S);
                }

                AnnexTable1ContractType contractInfo = factory.createAnnexTable1ContractType();
                if (!tfContractId.getText().isEmpty()) {
                    contractInfo.setContractId(tfContractId.getText().trim());
                }
                contractInfo.setContractName(cbContractName.getSelectedItem().toString());

                String energyCommodityType = cbEnergyCommodity.getSelectedItem().toString();
                if (energyCommodityType.equals("EL")) {
                    contractInfo.getEnergyCommodity().add(EnergyCommodityType.EL);
                } else {
                    contractInfo.getEnergyCommodity().add(EnergyCommodityType.NG);
                }
                String contractType = cbContractType.getSelectedItem().toString();
                if (contractType.equals("AU")) {
                    contractInfo.setContractType(ContractTypeType.AU);
                } else if (contractType.equals("CO")) {
                    contractInfo.setContractType(ContractTypeType.CO);
                } else if (contractType.equals("FW")) {
                    contractInfo.setContractType(ContractTypeType.FW);
                } else if (contractType.equals("OP")) {
                    contractInfo.setContractType(ContractTypeType.OP);
                } else if (contractType.equals("OP_FW")) {
                    contractInfo.setContractType(ContractTypeType.OP_FW);
                } else if (contractType.equals("OP_FU")) {
                    contractInfo.setContractType(ContractTypeType.OP_FU);
                } else if (contractType.equals("OP_SW")) {
                    contractInfo.setContractType(ContractTypeType.OP_SW);
                } else if (contractType.equals("SP")) {
                    contractInfo.setContractType(ContractTypeType.SP);
                } else if (contractType.equals("SW")) {
                    contractInfo.setContractType(ContractTypeType.SW);
                } else if (contractType.equals("OT")) {
                    contractInfo.setContractType(ContractTypeType.OT);
                } else {
                    contractInfo.setContractType(ContractTypeType.FU);
                }

                String settlementItem = cbSettlementMethod.getSelectedItem().toString();
                if (settlementItem.toUpperCase().equals("P")) {
                    contractInfo.setSettlementMethod(SettlementMethodType.P);
                } else if (settlementItem.toUpperCase().equals("C")) {
                    contractInfo.setSettlementMethod(SettlementMethodType.C);
                } else {
                    contractInfo.setSettlementMethod(SettlementMethodType.O);
                }

                OrganisedMarketPlaceType marketPlaceType = factory.createOrganisedMarketPlaceType();
                OrganisedMarketPlaceType marketPlaceType1 = factory.createOrganisedMarketPlaceType();
                String marketPlaceItem = cbOrganisedMarketPlace.getSelectedItem().toString();

                if (marketPlaceItem.toLowerCase().equals("lei")) {
                    marketPlaceType.setLei("LEI");
                } else if (marketPlaceItem.toLowerCase().equals("mic")) {
                    marketPlaceType.setMic("MIC");
                } else if (marketPlaceItem.toLowerCase().equals("acer")) {
                    marketPlaceType.setAce("ACER");
                } else {
                    marketPlaceType.setBil("XBIL");
                }
                contractInfo.setOrganisedMarketPlaceIdentifier(marketPlaceType);

                DeliveryPoint deliveryPointZone = (DeliveryPoint)cbDeliveryPoint.getSelectedItem();
                contractInfo.getDeliveryPointOrZone().add(deliveryPointZone.getCodeDelivery());
//                int delivery = cbDeliveryPoint.getSelectedIndex();
//                switch (delivery) {
//                    case 0:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HUNGARY);
//                        //    Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                        //     System.out.println(deliveryPointZone);
//                        break;
//                    case 1:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.BULGARIA);
//                        //  Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                        //  System.out.println(deliveryPointZone);
//                        break;
//                    case 2:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.SLOVAKIA);
//                        //Kontroler.getInstance().setDeliveryPoint(deliveryPointZone);
//                        // System.out.println(deliveryPointZone);
//                        break;
//
//                    case 3:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.IT_SL);
//
//                        break;
//                    case 4:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.GR_IT);
//
//                        break;
//                    case 5:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.GR_TR);
//
//                        break;
//                    case 6:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.GR_MK);
//
//                        break;
//                    case 7:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HR_SL);
//
//                        break;
//                    case 8:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HR_HU);
//
//                        break;
//                    case 9:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HR_BA);
//
//                        break;
//                    case 10:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HR_RS);
//
//                        break;
//                    case 11:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HU_RO);
//
//                        break;
//                    case 12:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HU_SK);
//
//                        break;
//                    case 13:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HU_UA);
//
//                        break;
//                    case 14:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HU_AT);
//
//                        break;
//                    case 15:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.HU_RS);
//
//                        break;
//                    case 16:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.BA_RS);
//
//                        break;
//                    case 17:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.ME_BA);
//
//                    case 18:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.ME_RS);
//
//                        break;
//                    case 19:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.ME_AL);
//
//                        break;
//
//                    case 20:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.AL_GR);
//
//                        break;
//                    case 21:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.AL_RS);
//
//                        break;
//
//                    case 22:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.BG_GR);
//
//                        break;
//                    case 23:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.BG_TR);
//
//                        break;
//                    case 24:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.BG_RS);
//
//                        break;
//                    case 25:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.MK_RS);
//
//                        break;
//                    case 26:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.RO_RS);
//
//                        break;
//                    case 27:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.RO_UA);
//
//                        break;
//                    case 28:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.RO_BG);
//
//                        break;
//                    case 29:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.CZ_SK);
//
//                        break;
//                    case 30:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.AT_CZ);
//
//                        break;
//                    case 31:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.AT_SL);
//
//                        break;
//                    case 32:
//                        contractInfo.getDeliveryPointOrZone().add(DeliveryPoint.APG_TENNET);
//
//                        break;
//                }
//                if (!tfdeliveryPoint.getText().isEmpty()) {
//                    String deliveryPoint = tfdeliveryPoint.getText().trim();
//                    contractInfo.getDeliveryPointOrZone().add(deliveryPoint);
//                }

                String startDate = ((JTextField) jcalStartDate.getDateEditor().getUiComponent()).getText();
                System.out.println(startDate);
                String endDate = ((JTextField) jCalEnddate.getDateEditor().getUiComponent()).getText();

                if (!startDate.isEmpty()) {
                    XMLGregorianCalendar xmlDate2 = stringToXMLGregorianCalendar(startDate);
                    contractInfo.setDeliveryStartDate(xmlDate2);
                } else {
                    throw new Exception("Morate da izaberete start date");
                    //JOptionPane.showMessageDialog(this, "Morate da izaberete start date", "Greska!", JOptionPane.ERROR_MESSAGE);
                }

                if (!endDate.isEmpty()) {

                    XMLGregorianCalendar xmlDate3 = stringToXMLGregorianCalendar(endDate);
                    contractInfo.setDeliveryEndDate(xmlDate3);
                } else {
                    throw new Exception("Morate da izaberete end date");
                }

                String loadItem = cbLoadType.getSelectedItem().toString();
                if (loadItem.toUpperCase().equals("BL")) {
                    contractInfo.setLoadType(ContractLoadType.BL);
                } else if (loadItem.toUpperCase().equals("PL")) {
                    contractInfo.setLoadType(ContractLoadType.PL);
                } else if (loadItem.toUpperCase().equals("OP")) {
                    contractInfo.setLoadType(ContractLoadType.OP);
                } else if (loadItem.toUpperCase().equals("BH")) {
                    contractInfo.setLoadType(ContractLoadType.BH);
                } else if (loadItem.toUpperCase().equals("SH")) {
                    contractInfo.setLoadType(ContractLoadType.SH);
                } else if (loadItem.toUpperCase().equals("GD")) {
                    contractInfo.setLoadType(ContractLoadType.GD);
                } else {
                    contractInfo.setLoadType(ContractLoadType.OT);
                }

                DeliveryProfileDetails deliveryProfile = factory.createDeliveryProfileDetails();

                XMLGregorianCalendar startDate1 = stringToXMLGregorianCalendarZOne(tfStartTime.getText().trim());
                XMLGregorianCalendar end2 = stringToXMLGregorianCalendarZOne(tfEndTime.getText().trim());
                JAXBElement<XMLGregorianCalendar> element = factory.createDeliveryProfileDetailsLoadDeliveryStartTime(startDate1);
                JAXBElement<XMLGregorianCalendar> element2 = factory.createDeliveryProfileDetailsLoadDeliveryEndTime(end2);

                deliveryProfile.getLoadDeliveryStartTimeAndLoadDeliveryEndTime().add(element);
                deliveryProfile.getLoadDeliveryStartTimeAndLoadDeliveryEndTime().add(element2);
                if (!cbDaysOfWeek.getSelectedItem().toString().equals("")) {
                    deliveryProfile.getDaysOfTheWeek().add(cbDaysOfWeek.getSelectedItem().toString());
                }
                contractInfo.getDeliveryProfile().clear();
                contractInfo.getDeliveryProfile().add(deliveryProfile);

                // if(!tfTransactionTime.getText().isEmpty()){
                XMLGregorianCalendar transactionTime = stringToXMLTransactionDate(tfTransactionTime.getText());
                table.setTransactionTime(transactionTime);

                TradeIdType tradeId = factory.createTradeIdType();
                if (!tfUniqueTransId.getText().isEmpty()) {
                    tradeId.setUniqueTransactionIdentifier(tfUniqueTransId.getText().trim());
                    table.setUniqueTransactionIdentifier(tradeId);
                } else {
                    table.setUniqueTransactionIdentifier(null);
                }
                
                String linkedTransactionId=tfLinkedTransactionId.getText().trim();
                  if (!linkedTransactionId.isEmpty()) {
                      table.getLinkedTransactionId().clear();
                    table.getLinkedTransactionId().add(linkedTransactionId);
                } else {
                     table.getLinkedTransactionId().add(null);
                }

                String marketPlaceItem1 = cbOrganisedMarketPlace1.getSelectedItem().toString();

                if (marketPlaceItem1.toLowerCase().equals("lei")) {
                    marketPlaceType1.setLei("LEI");
                } else if (marketPlaceItem1.toLowerCase().equals("mic")) {
                    marketPlaceType1.setMic("MIC");
                } else if (marketPlaceItem1.toLowerCase().equals("acer")) {
                    marketPlaceType1.setAce("ACER");
                } else {
                    marketPlaceType1.setBil("XBIL");
                }

                table.setOrganisedMarketPlaceIdentifier(marketPlaceType1);

                String actionType = cbActionType.getSelectedItem().toString();
                if (actionType.toLowerCase().equals("e")) {
                    table.setActionType(ActionTypesType.E);
                } else if (actionType.toLowerCase().equals("c")) {
                    table.setActionType(ActionTypesType.C);
                } else if (actionType.toLowerCase().equals("m")) {
                    table.setActionType(ActionTypesType.M);
                } else {
                    table.setActionType(ActionTypesType.N);
                }

                PriceDetailsType priceType = factory.createPriceDetailsType();
                priceType.setPrice(new BigDecimal(tfPrice.getText()));

                String priceCurrencyType = cbCurrency.getSelectedItem().toString();
                if (priceCurrencyType.toUpperCase().equals("EUR")) {
                    priceType.setPriceCurrency(CurrencyCodeType.EUR);
                } else if (priceCurrencyType.toUpperCase().equals("BGN")) {
                    priceType.setPriceCurrency(CurrencyCodeType.BGN);
                } else if (priceCurrencyType.toUpperCase().equals("CHF")) {
                    priceType.setPriceCurrency(CurrencyCodeType.CHF);
                } else if (priceCurrencyType.toUpperCase().equals("CZK")) {
                    priceType.setPriceCurrency(CurrencyCodeType.CZK);
                } else if (priceCurrencyType.toUpperCase().equals("DKK")) {
                    priceType.setPriceCurrency(CurrencyCodeType.DKK);
                } else if (priceCurrencyType.toUpperCase().equals("EUX")) {
                    priceType.setPriceCurrency(CurrencyCodeType.EUX);
                } else if (priceCurrencyType.toUpperCase().equals("GBP")) {
                    priceType.setPriceCurrency(CurrencyCodeType.GBP);
                } else if (priceCurrencyType.toUpperCase().equals("GBX")) {
                    priceType.setPriceCurrency(CurrencyCodeType.GBX);
                } else if (priceCurrencyType.toUpperCase().equals("HRK")) {
                    priceType.setPriceCurrency(CurrencyCodeType.HRK);
                } else if (priceCurrencyType.toUpperCase().equals("HUF")) {
                    priceType.setPriceCurrency(CurrencyCodeType.HUF);
                } else if (priceCurrencyType.toUpperCase().equals("ISK")) {
                    priceType.setPriceCurrency(CurrencyCodeType.ISK);
                } else if (priceCurrencyType.toUpperCase().equals("NOK")) {
                    priceType.setPriceCurrency(CurrencyCodeType.NOK);
                } else if (priceCurrencyType.toUpperCase().equals("PCT")) {
                    priceType.setPriceCurrency(CurrencyCodeType.PCT);
                } else if (priceCurrencyType.toUpperCase().equals("PLN")) {
                    priceType.setPriceCurrency(CurrencyCodeType.PLN);
                } else if (priceCurrencyType.toUpperCase().equals("RON")) {
                    priceType.setPriceCurrency(CurrencyCodeType.RON);
                } else if (priceCurrencyType.toUpperCase().equals("SEK")) {
                    priceType.setPriceCurrency(CurrencyCodeType.SEK);
                } else {
                    priceType.setPriceCurrency(CurrencyCodeType.USD);
                }

                table.setPriceDetails(priceType);

                NotionalAmountDetailsType notionalDetailsType = factory.createNotionalAmountDetailsType();
                notionalDetailsType.setNotionalAmount(new BigDecimal(tfNotionalAmount.getText()));

                String notionalCurrency = tfNotionalCurrency.getText();
                if (notionalCurrency.toUpperCase().equals("EUR")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.EUR);
                } else if (notionalCurrency.toUpperCase().equals("BGN")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.BGN);
                } else if (notionalCurrency.toUpperCase().equals("CHF")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.CHF);
                } else if (notionalCurrency.toUpperCase().equals("CZK")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.CZK);
                } else if (notionalCurrency.toUpperCase().equals("DKK")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.DKK);
                } else if (notionalCurrency.toUpperCase().equals("EUX")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.EUX);
                } else if (notionalCurrency.toUpperCase().equals("GBP")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.GBP);
                } else if (notionalCurrency.toUpperCase().equals("GBX")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.GBX);
                } else if (notionalCurrency.toUpperCase().equals("HRK")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.HRK);
                } else if (notionalCurrency.toUpperCase().equals("HUF")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.HUF);
                } else if (notionalCurrency.toUpperCase().equals("ISK")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.ISK);
                } else if (notionalCurrency.toUpperCase().equals("NOK")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.NOK);
                } else if (notionalCurrency.toUpperCase().equals("PCT")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.PCT);
                } else if (notionalCurrency.toUpperCase().equals("PLN")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.PLN);
                } else if (notionalCurrency.toUpperCase().equals("RON")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.RON);
                } else if (notionalCurrency.toUpperCase().equals("SEK")) {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.SEK);
                } else {
                    notionalDetailsType.setNotionalCurrency(CurrencyCodeType.USD);
                }
                table.setNotionalAmountDetails(notionalDetailsType);

                QuantityType quantityType = factory.createQuantityType();
                quantityType.setValue(new BigDecimal(tfQuantityValue.getText()));

                String quantityUnit = cbQuantityUnit.getSelectedItem().toString();
                if (quantityUnit.equals("KW")) {
                    quantityType.setUnit(QuantityUnitType.KW);
                } else if (quantityUnit.equals("MW")) {
                    quantityType.setUnit(QuantityUnitType.MW);
                } else if (quantityUnit.equals("cm/d")) {
                    quantityType.setUnit(QuantityUnitType.CM_D);
                } else if (quantityUnit.equals("GW")) {
                    quantityType.setUnit(QuantityUnitType.GW);
                } else if (quantityUnit.equals("GWh/d")) {
                    quantityType.setUnit(QuantityUnitType.G_WH_D);
                } else if (quantityUnit.equals("GWh/h")) {
                    quantityType.setUnit(QuantityUnitType.G_WH_H);
                } else if (quantityUnit.equals("KTherm/d")) {
                    quantityType.setUnit(QuantityUnitType.K_THERM_D);
                } else if (quantityUnit.equals("KWh/d")) {
                    quantityType.setUnit(QuantityUnitType.K_WH_D);
                } else if (quantityUnit.equals("KWh/h")) {
                    quantityType.setUnit(QuantityUnitType.K_WH_H);
                } else if (quantityUnit.equals("mcm/d")) {
                    quantityType.setUnit(QuantityUnitType.MCM_D);
                } else if (quantityUnit.equals("MTherm/d")) {
                    quantityType.setUnit(QuantityUnitType.M_THERM_D);
                } else if (quantityUnit.equals("MWh/d")) {
                    quantityType.setUnit(QuantityUnitType.M_WH_D);
                } else if (quantityUnit.equals("MWh/h")) {
                    quantityType.setUnit(QuantityUnitType.M_WH_H);
                } else {
                    quantityType.setUnit(QuantityUnitType.THERM_D);
                }
                table.setQuantity(quantityType);

                NotionalQuantityType totalNotionalType = factory.createNotionalQuantityType();
                totalNotionalType.setValue(new BigDecimal(tfTotalNValue.getText()));

                String tquantityUnit = cbTotNotionalUnit.getSelectedItem().toString();
                if (tquantityUnit.equals("cm")) {
                    totalNotionalType.setUnit(NotionalQuantityUnitType.CM);
                } else if (tquantityUnit.equals("GWh")) {
                    totalNotionalType.setUnit(NotionalQuantityUnitType.G_WH);
                } else if (tquantityUnit.equals("KTherm")) {
                    totalNotionalType.setUnit(NotionalQuantityUnitType.K_THERM);
                } else if (tquantityUnit.equals("KWh")) {
                    totalNotionalType.setUnit(NotionalQuantityUnitType.K_WH);
                } else if (tquantityUnit.equals("mcm")) {
                    totalNotionalType.setUnit(NotionalQuantityUnitType.MCM);
                } else if (tquantityUnit.equals("MTherm")) {
                    totalNotionalType.setUnit(NotionalQuantityUnitType.M_THERM);
                } else if (tquantityUnit.equals("Therm")) {
                    totalNotionalType.setUnit(NotionalQuantityUnitType.THERM);
                } else {
                    totalNotionalType.setUnit(NotionalQuantityUnitType.M_WH);
                }
                table.setTotalNotionalContractQuantity(totalNotionalType);

                ContractInfoType1 contractInfoType1 = factory.createContractInfoType1();
                contractInfoType1.setContract(contractInfo);
                table.setContractInfo(contractInfoType1);

                TradeListType tradeList = factory.createTradeListType();
                tradeList.getTradeReport().add(table);
//                Kontroler.getInstance().updateReport(table);

                ReportingEntityID entityID = factory.createReportingEntityID();
                entityID.setAce("B0000450I.HU");
                REMITTable1 table1 = factory.createREMITTable1();
                table1.setReportingEntityID(entityID);
                table1.setTradeList(tradeList);

                JAXBContext context = JAXBContext.newInstance(table1.getClass().getPackage().getName());
                //    JAXBElement <REMITTable1> element=factory.createREMITTable1(table1);
                Marshaller marshaller = context.createMarshaller();
                marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
                String userHomeFolder = System.getProperty("user.home");

                File textFile = new File(userHomeFolder + "/Desktop", "RemitTable1.xml");
                // File file = new File("Desktop/RemitTable1.xml");
                marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.acer.europa.eu/REMIT/REMITTable1_V1.xsd");
                //   marshaller.marshal(table1, System.out);
                marshaller.marshal(table1, textFile);

                //  JOptionPane.showMessageDialog(this, "Your changes are updated", "Success!", 1);
                Window w = SwingUtilities.getWindowAncestor(PanelRemitTable1.this);
                w.setVisible(false);

            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Greška", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_butUpdateActionPerformed

    private void cbMarketParticipantItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbMarketParticipantItemStateChanged

        if (cbMarketParticipant.getSelectedItem().equals("ace")) {
            tfOtherMarketParticipant.setVisible(false);
            cbAcerCodes.setVisible(true);
        } else {
            tfOtherMarketParticipant.setVisible(true);
            cbAcerCodes.setVisible(false);
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_cbMarketParticipantItemStateChanged

    private void tfNotionalAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfNotionalAmountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfNotionalAmountActionPerformed

    private void initCombo() {

        String[] items = {"ace", "bic", "eic", "gln", "lei"};
        cbMarketParticipant.setModel(new javax.swing.DefaultComboBoxModel(items));
        cbAcerCodes.setModel(new DefaultComboBoxModel(Kontroler.getInstance().getAccerCodes().toArray()));
       //  cbAcerCodes.setModel(new DefaultComboBoxModel<>(new String[]{"ELPETRA BG", "KOZLODUY BG", "ELEPTRA CY", "PPC GR", "HERON GR", "ELPEDISON GR", "PROTERGIA GR", "EDF HU", "Europe Energy IT", "EDS RO", "AYEN SI", "EDS SK", "SEE POWER BG", "TPP Maritsa East 2 BG", "Transenergo BG", "NEAS Energy DK", "PETROL SI", "ENERGANA CZ", "ENERGO PRO BG", "EDS HR"}));
        if (cbMarketParticipant.getItemAt(0).equals("ace")) {
            tfOtherMarketParticipant.setVisible(false);
            cbAcerCodes.setVisible(true);
        } else {
            tfOtherMarketParticipant.setVisible(true);
            cbAcerCodes.setVisible(false);
        }

        cbTradingCapacity.setModel(new DefaultComboBoxModel<>(new String[]{"A", "P"}));
        cbBuysellIndicator.setModel(new DefaultComboBoxModel<>(new String[]{"B", "S", "C"}));
        cbMarketParticipant.setModel(new DefaultComboBoxModel<>(new String[]{"ace", "bic", "eic", "gln", "lei"}));
        //cbContractType.setModel(new DefaultComboBoxModel<>(new String[]{"AUC", "CON", "FWD", "FUT", "OPT", "OPT_FWD", "OPT_FUT", "OPT_SWP", "SPO", "SWP", "OTH"}));
        cbContractType.setModel(new DefaultComboBoxModel<>(new String[]{"AU", "CO", "FW", "FU", "OP", "OP_FW", "OP_FU", "OP_SW", "SP", "SW", "OT"}));
        cbEnergyCommodity.setModel(new DefaultComboBoxModel<>(new String[]{"NG", "EL"}));
        cbSettlementMethod.setModel(new DefaultComboBoxModel<>(new String[]{"P", "C", "O"}));
        cbOrganisedMarketPlace.setModel(new DefaultComboBoxModel<>(new String[]{"LEI", "MIC", "ACER", "XBIL"}));
        cbOrganisedMarketPlace1.setModel(new DefaultComboBoxModel<>(new String[]{"LEI", "MIC", "ACER", "XBIL"}));
        cbLoadType.setModel(new DefaultComboBoxModel<>(new String[]{"BL", "PL", "OP", "BH", "SH", "GD", "OT"}));
        cbDaysOfWeek.setModel(new DefaultComboBoxModel<>(new String[]{"", "MO", "TU", "WE", "TH", "FR", "SA", "SU", "XB", "IB", "WD", "WN"}));
        cbContractName.setModel(new DefaultComboBoxModel<>(new String[]{"BILCONTRACT", "BACKLOADING", "EXECUTION"}));
        cbActionType.setModel(new DefaultComboBoxModel<>(new String[]{"N", "M", "E", "C"}));
        cbCurrency.setModel(new DefaultComboBoxModel<>(new String[]{"BGN", "CHF", "CZK", "DKK", "EUR", "EUX", "GBX", "GBP", "HRK", "HUF", "ISK", "NOK", "PCT", "PLN", "RON", "SEK", "USD"}));
        String[] unitItems = {NotionalQuantityUnitType.CM.value(), NotionalQuantityUnitType.G_WH.value(), NotionalQuantityUnitType.K_THERM.value(), NotionalQuantityUnitType.K_WH.value(), NotionalQuantityUnitType.MCM.value(), NotionalQuantityUnitType.M_THERM.value(), NotionalQuantityUnitType.M_WH.value(), NotionalQuantityUnitType.THERM.value()};
        cbTotNotionalUnit.setModel(new DefaultComboBoxModel<>(unitItems));
        cbQuantityUnit.setModel(new DefaultComboBoxModel<>(new String[]{QuantityUnitType.CM_D.value(), QuantityUnitType.GW.value(), QuantityUnitType.G_WH_D.value(), QuantityUnitType.G_WH_H.value(), QuantityUnitType.KW.value(), QuantityUnitType.K_THERM_D.value(), QuantityUnitType.K_WH_D.value(), QuantityUnitType.K_WH_H.value(), QuantityUnitType.MCM_D.value(), QuantityUnitType.MW.value(), QuantityUnitType.M_THERM_D.value(), QuantityUnitType.M_WH_D.value(), QuantityUnitType.M_WH_H.value(), QuantityUnitType.THERM_D.value()}));
      cbDeliveryPoint.setModel(new DefaultComboBoxModel(Kontroler.getInstance().getDeliveryPointsList().toArray()));
        
        //cbDeliveryPoint.setModel(new DefaultComboBoxModel<>(new String[]{"HUNGARY", "BULGARIA", "SLOVAKIA", "IT-SL",
         //   "GR-IT", "GR-TR", "GR-MK", "HR-SL", "HR-HU", "HR-BA", "HR-RS", "HU-RO", "HU-SK", "HU-UA", "HU-AT", "HU-RS", "BA-RS", "ME-BA", "ME-RS", "ME-AL", "AL-GR", "AL-RS", "BG-GR", "BG-TR", "BG-RS", "MK-RS", "RO-RS", "RO-UA", "RO-BG", "CZ-SK", "AT-CZ", "AT-SL", "APG-TENNET"}));
    }

    private void initValues() {

        setRbr();
        int recordNumber = 0;
        if (Kontroler.getInstance().vratiSveTradeListe() == null) {
            recordNumber = 1;
            return;
        } else {
            recordNumber = Kontroler.getInstance().vratiSveTradeListe().size();
            recordNumber++;
            System.out.println("rec" + recordNumber);
        }
        tfRecordNumber.setText("" + recordNumber);
        tfStartTime.setText("00:00:00");
        tfEndTime.setText("00:00:00");
        tfTransactionTime.setText("" + ZonedDateTime.now(ZoneOffset.UTC).withNano(0).toString());
        butUpdate.setVisible(false);
    }

    public XMLGregorianCalendar stringToXMLGregorianCalendar(String s)
            throws ParseException,
            DatatypeConfigurationException {
        XMLGregorianCalendar result = null;
        Date date;
        SimpleDateFormat simpleDateFormat;

        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        date = simpleDateFormat.parse(s);
        String str = new SimpleDateFormat("yyyy-MM-dd").format(date);
        result = DatatypeFactory.newInstance().newXMLGregorianCalendar(str);

        return result;
    }

    public XMLGregorianCalendar stringToXMLGregorianCalendarZOne(String s)
            throws
            DatatypeConfigurationException {
        XMLGregorianCalendar result = null;
        try {

            Date date;
            SimpleDateFormat simpleDateFormat;
            GregorianCalendar gregorianCalendar;

            DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
            java.sql.Time timeValue = new java.sql.Time(formatter.parse(s).getTime());
            System.out.println(timeValue);

//        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//        date = simpleDateFormat.parse(s);
//         DateFormat formatter = DateFormat.getTimeInstance();        // time only
            String str = formatter.format(timeValue);
            result = DatatypeFactory.newInstance().newXMLGregorianCalendar(str);

        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(this, "Morate da unesete vreme u formatu 00:00:00", "Greska!", JOptionPane.ERROR_MESSAGE);
        }
        return result;
    }

    public XMLGregorianCalendar stringToXMLTransactionDate(String s)
            throws ParseException,
            DatatypeConfigurationException {
        XMLGregorianCalendar result = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
//sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = sdf.parse(s); //prints-> Mon Sep 30 02:46:19 CST 2013
        String time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(date);

        System.out.println("Transaction time" + time);
        DateTimeFormatter f = DateTimeFormatter.ISO_DATE_TIME;
        ZonedDateTime zdt = ZonedDateTime.parse(s.trim(), f);
        result = DatatypeFactory.newInstance().newXMLGregorianCalendar(time);

        return result;
    }

    public void srediFormu(AnnexTable1Trade trade) {
        this.trade = trade;
        buttonGenerateXML.setVisible(false);
        //tfRecordNumber.setEditable(false);

        tfRecordNumber.setText("" + trade.getRecordSeqNumber());
        String[] items = {"ace", "bic", "eic", "gln", "lei"};
//        tfIdOfMarketParticipant.setText(trade.getIdOfMarketParticipant().getAce());
        if (trade.getTraderID() == null) {
            tfTraderForMarketParticipant.setText("");
            // return;
        } else {
            tfTraderForMarketParticipant.setText(trade.getTraderID().getTraderIdForMarketParticipant());
        }
        // System.out.println("tip"+trade.getOtherMarketParticipant());

        if (trade.getOtherMarketParticipant().getAce() != null) {
            cbMarketParticipant.setSelectedItem("ace");
            //cbAcerCodes.setSelectedItem(Kontroler.getInstance().getCompany());
            cbAcerCodes.setSelectedItem(Kontroler.getInstance().getCompanyFromAcerCode(trade.getOtherMarketParticipant().getAce()));
            System.out.println("acer code for company" + Kontroler.getInstance().getCompanyFromAcerCode(trade.getOtherMarketParticipant().getAce()));

        } else if (trade.getOtherMarketParticipant().getBic() != null) {
            cbMarketParticipant.setSelectedItem("bic");
            tfOtherMarketParticipant.setText(trade.getOtherMarketParticipant().getBic());
        } else if (trade.getOtherMarketParticipant().getEic() != null) {
            cbMarketParticipant.setSelectedItem("eic");
            tfOtherMarketParticipant.setText(trade.getOtherMarketParticipant().getEic());
        } else if (trade.getOtherMarketParticipant().getGln() != null) {
            cbMarketParticipant.setSelectedItem("gln");
            tfOtherMarketParticipant.setText(trade.getOtherMarketParticipant().getGln());
        } else {
            cbMarketParticipant.setSelectedItem("lei");
            tfOtherMarketParticipant.setText(trade.getOtherMarketParticipant().getLei());
        }

        cbBuysellIndicator.setSelectedItem(trade.getBuySellIndicator().value());
        cbTradingCapacity.setSelectedItem(trade.getTradingCapacity().value());
        cbContractName.setSelectedItem(trade.getContractInfo().getContract().getContractName());
        cbContractType.setSelectedItem(trade.getContractInfo().getContract().getContractType().value());
        System.out.println(trade.getContractInfo().getContract().getContractType().value());
        cbSettlementMethod.setSelectedItem(trade.getContractInfo().getContract().getSettlementMethod().value());
        cbEnergyCommodity.setSelectedItem(trade.getContractInfo().getContract().getEnergyCommodity().get(0).value());
        cbActionType.setSelectedItem(trade.getActionType().value());
        cbLoadType.setSelectedItem(trade.getContractInfo().getContract().getLoadType().value());
        cbDeliveryPoint.setSelectedItem(Kontroler.getInstance().getDeliveryPointFromCode(trade.getContractInfo().getContract().getDeliveryPointOrZone().get(0)));

        if (trade.getBeneficiaryIdentification() == null) {
            tfBeneficiaryId.setText("");
        } else {
            tfBeneficiaryId.setText(trade.getBeneficiaryIdentification().getAce());
        }
        tfTransactionTime.setText(trade.getTransactionTime().toString());
        if (trade.getContractInfo().getContract().getContractId() != null) {
            tfContractId.setText(trade.getContractInfo().getContract().getContractId());
        }

        // if(trade.getContractInfo().getContract().getDeliveryPointOrZone()!=null){
//        try {
//            String del = trade.getContractInfo().getContract().getDeliveryPointOrZone().get(0);
//            tfdeliveryPoint.setText(del);
//        } catch (IndexOutOfBoundsException ex) {
//            tfdeliveryPoint.setText("");
//        }
        // }
        XMLGregorianCalendar cal = trade.getContractInfo().getContract().getDeliveryStartDate();
        Date startDate = cal.toGregorianCalendar().getTime();
        XMLGregorianCalendar calEnd = trade.getContractInfo().getContract().getDeliveryEndDate();
        Date endDate = calEnd.toGregorianCalendar().getTime();
        jcalStartDate.setDate(startDate);
        jCalEnddate.setDate(endDate);

        cbCurrency.setSelectedItem(trade.getPriceDetails().getPriceCurrency().value());
        cbQuantityUnit.setSelectedItem(trade.getQuantity().getUnit().value());
        cbTotNotionalUnit.setSelectedItem(trade.getTotalNotionalContractQuantity().getUnit().value());

        try {
            cbDaysOfWeek.setSelectedItem(trade.getContractInfo().getContract().getDeliveryProfile().get(0).getDaysOfTheWeek().get(0));
        } catch (IndexOutOfBoundsException exc) {
            cbDaysOfWeek.setSelectedItem("");
        }

        if (trade.getContractInfo().getContract().getDeliveryProfile().get(0) != null) {
            System.out.println("delivery" + trade.getContractInfo().getContract().getDeliveryProfile().get(0).getLoadDeliveryStartTimeAndLoadDeliveryEndTime());
            //    DeliveryProfileDetails delivery=trade.getContractInfo().getContract().getDeliveryProfile().get(0);
            List<JAXBElement<XMLGregorianCalendar>> elements = trade.getContractInfo().getContract().getDeliveryProfile().get(0).getLoadDeliveryStartTimeAndLoadDeliveryEndTime();

            JAXBElement<XMLGregorianCalendar> elStartTime = elements.get(0);
            JAXBElement<XMLGregorianCalendar> elEndTime = elements.get(1);

            XMLGregorianCalendar calST = elStartTime.getValue();
            System.out.println("time" + calST.toXMLFormat());

            tfStartTime.setText(calST.toXMLFormat());
            tfEndTime.setText(elEndTime.getValue().toXMLFormat());
            //     }

            if (trade.getUniqueTransactionIdentifier() == null) {
                tfUniqueTransId.setText("");
            } else {
                tfUniqueTransId.setText(trade.getUniqueTransactionIdentifier().getUniqueTransactionIdentifier());
            }
            
                if (trade.getLinkedTransactionId() == null || trade.getLinkedTransactionId().isEmpty()) {
                tfLinkedTransactionId.setText("");
            } else {
                tfLinkedTransactionId.setText(trade.getLinkedTransactionId().get(0));
            }
            
            

            tfPrice.setText("" + trade.getPriceDetails().getPrice());
            tfNotionalAmount.setText("" + trade.getNotionalAmountDetails().getNotionalAmount());
            tfNotionalCurrency.setText(trade.getNotionalAmountDetails().getNotionalCurrency().value());
            tfQuantityValue.setText("" + trade.getQuantity().getValue());
            tfTotalNValue.setText("" + trade.getTotalNotionalContractQuantity().getValue());

            if (trade.getOrganisedMarketPlaceIdentifier().getAce() != null) {
                cbOrganisedMarketPlace1.setSelectedItem("ACER");
            } else if (trade.getOrganisedMarketPlaceIdentifier().getBil() != null) {
                cbOrganisedMarketPlace1.setSelectedItem("XBIL");
            } else if (trade.getOrganisedMarketPlaceIdentifier().getLei() != null) {
                cbOrganisedMarketPlace1.setSelectedItem("LEI");
            } else {
                cbOrganisedMarketPlace1.setSelectedItem("MIC");
            }

            if (trade.getContractInfo().getContract().getOrganisedMarketPlaceIdentifier().getAce() != null) {
                cbOrganisedMarketPlace.setSelectedItem("ACER");
            } else if (trade.getContractInfo().getContract().getOrganisedMarketPlaceIdentifier().getBil() != null) {
                cbOrganisedMarketPlace.setSelectedItem("XBIL");
            } else if (trade.getContractInfo().getContract().getOrganisedMarketPlaceIdentifier().getLei() != null) {
                cbOrganisedMarketPlace.setSelectedItem("LEI");
            } else {
                cbOrganisedMarketPlace.setSelectedItem("MIC");
            }

        }
    }

    public void smanjiRN() {
        rbr--;
    }

    private void addItemInCombo(JComboBox<String> combo, String item) {

        int br = 0;
        for (int i = 0; i < combo.getItemCount(); i++) {
            String kCombo = combo.getItemAt(i);
            System.out.println("kmbo" + kCombo);

            if (item.equals(kCombo)) {
                br++;
            }
        }
        if (br == 0) {
            combo.addItem(item);
        }

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton butUpdate;
    private javax.swing.JButton buttonGenerateXML;
    private javax.swing.JComboBox<String> cbAcerCodes;
    private javax.swing.JComboBox<String> cbActionType;
    private javax.swing.JComboBox<String> cbBuysellIndicator;
    private javax.swing.JComboBox<String> cbContractName;
    private javax.swing.JComboBox<String> cbContractType;
    private javax.swing.JComboBox<String> cbCurrency;
    private javax.swing.JComboBox<String> cbDaysOfWeek;
    private javax.swing.JComboBox<String> cbDeliveryPoint;
    private javax.swing.JComboBox<String> cbEnergyCommodity;
    private javax.swing.JComboBox<String> cbLoadType;
    private javax.swing.JComboBox<String> cbMarketParticipant;
    private javax.swing.JComboBox<String> cbOrganisedMarketPlace;
    private javax.swing.JComboBox<String> cbOrganisedMarketPlace1;
    private javax.swing.JPanel cbOrganizedMarketPlace;
    private javax.swing.JComboBox<String> cbQuantityUnit;
    private javax.swing.JComboBox<String> cbSettlementMethod;
    private javax.swing.JComboBox<String> cbTotNotionalUnit;
    private javax.swing.JComboBox<String> cbTradingCapacity;
    private com.toedter.calendar.JDateChooser jCalEnddate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private com.toedter.calendar.JDateChooser jcalStartDate;
    private javax.swing.JTextField tfBeneficiaryId;
    private javax.swing.JTextField tfContractId;
    private javax.swing.JTextField tfEndTime;
    private javax.swing.JTextField tfLinkedTransactionId;
    private javax.swing.JTextField tfNotionalAmount;
    private javax.swing.JTextField tfNotionalCurrency;
    private javax.swing.JTextField tfOtherMarketParticipant;
    private javax.swing.JTextField tfPrice;
    private javax.swing.JTextField tfQuantityValue;
    private javax.swing.JTextField tfRecordNumber;
    private javax.swing.JTextField tfStartTime;
    private javax.swing.JTextField tfTotalNValue;
    private javax.swing.JTextField tfTraderForMarketParticipant;
    private javax.swing.JTextField tfTransactionTime;
    private javax.swing.JTextField tfUniqueTransId;
    // End of variables declaration//GEN-END:variables

    @Override
    public void windowOpened(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosing(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosed(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowIconified(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowActivated(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        setCloseRbr();
        System.out.println("Deaktiviran");
    }

    public static double roundToDecimals(double d, int c) {
        int temp = (int) (d * Math.pow(10, c));
        return ((double) temp) / Math.pow(10, c);
    }
}
