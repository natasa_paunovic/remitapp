//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.09.07 at 05:14:08 PM CEST 
//


package eu.europa.acer.remit.remittable1_v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for quantityUnitType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="quantityUnitType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="KW"/>
 *     &lt;enumeration value="KWh/h"/>
 *     &lt;enumeration value="KWh/d"/>
 *     &lt;enumeration value="MW"/>
 *     &lt;enumeration value="MWh/h"/>
 *     &lt;enumeration value="MWh/d"/>
 *     &lt;enumeration value="GW"/>
 *     &lt;enumeration value="GWh/h"/>
 *     &lt;enumeration value="GWh/d"/>
 *     &lt;enumeration value="Therm/d"/>
 *     &lt;enumeration value="KTherm/d"/>
 *     &lt;enumeration value="MTherm/d"/>
 *     &lt;enumeration value="cm/d"/>
 *     &lt;enumeration value="mcm/d"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "quantityUnitType")
@XmlEnum
public enum QuantityUnitType {

    KW("KW"),
    @XmlEnumValue("KWh/h")
    K_WH_H("KWh/h"),
    @XmlEnumValue("KWh/d")
    K_WH_D("KWh/d"),
    MW("MW"),
    @XmlEnumValue("MWh/h")
    M_WH_H("MWh/h"),
    @XmlEnumValue("MWh/d")
    M_WH_D("MWh/d"),
    GW("GW"),
    @XmlEnumValue("GWh/h")
    G_WH_H("GWh/h"),
    @XmlEnumValue("GWh/d")
    G_WH_D("GWh/d"),
    @XmlEnumValue("Therm/d")
    THERM_D("Therm/d"),
    @XmlEnumValue("KTherm/d")
    K_THERM_D("KTherm/d"),
    @XmlEnumValue("MTherm/d")
    M_THERM_D("MTherm/d"),
    @XmlEnumValue("cm/d")
    CM_D("cm/d"),
    @XmlEnumValue("mcm/d")
    MCM_D("mcm/d");
    private final String value;

    QuantityUnitType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static QuantityUnitType fromValue(String v) {
        for (QuantityUnitType c: QuantityUnitType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
