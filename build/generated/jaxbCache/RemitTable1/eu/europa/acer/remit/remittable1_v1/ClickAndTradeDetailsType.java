//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.09.07 at 05:14:08 PM CEST 
//


package eu.europa.acer.remit.remittable1_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for clickAndTradeDetailsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="clickAndTradeDetailsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orderType" type="{http://www.acer.europa.eu/REMIT/REMITTable1_V1.xsd}orderTypesType" maxOccurs="unbounded"/>
 *         &lt;element name="orderCondition" type="{http://www.acer.europa.eu/REMIT/REMITTable1_V1.xsd}orderConditionsType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="orderStatus" type="{http://www.acer.europa.eu/REMIT/REMITTable1_V1.xsd}orderStatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="minimumExecuteVolume" type="{http://www.acer.europa.eu/REMIT/REMITTable1_V1.xsd}quantityType" minOccurs="0"/>
 *         &lt;element name="triggerDetails" type="{http://www.acer.europa.eu/REMIT/REMITTable1_V1.xsd}triggerDetailsType" minOccurs="0"/>
 *         &lt;element name="undisclosedVolume" type="{http://www.acer.europa.eu/REMIT/REMITTable1_V1.xsd}quantityType" minOccurs="0"/>
 *         &lt;element name="orderDuration" type="{http://www.acer.europa.eu/REMIT/REMITTable1_V1.xsd}orderDurationType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clickAndTradeDetailsType", propOrder = {
    "orderType",
    "orderCondition",
    "orderStatus",
    "minimumExecuteVolume",
    "triggerDetails",
    "undisclosedVolume",
    "orderDuration"
})
public class ClickAndTradeDetailsType {

    @XmlElement(required = true)
    protected List<OrderTypesType> orderType;
    protected List<OrderConditionsType> orderCondition;
    protected List<OrderStatusType> orderStatus;
    protected QuantityType minimumExecuteVolume;
    protected TriggerDetailsType triggerDetails;
    protected QuantityType undisclosedVolume;
    protected OrderDurationType orderDuration;

    /**
     * Gets the value of the orderType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderTypesType }
     * 
     * 
     */
    public List<OrderTypesType> getOrderType() {
        if (orderType == null) {
            orderType = new ArrayList<OrderTypesType>();
        }
        return this.orderType;
    }

    /**
     * Gets the value of the orderCondition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderCondition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderCondition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderConditionsType }
     * 
     * 
     */
    public List<OrderConditionsType> getOrderCondition() {
        if (orderCondition == null) {
            orderCondition = new ArrayList<OrderConditionsType>();
        }
        return this.orderCondition;
    }

    /**
     * Gets the value of the orderStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderStatusType }
     * 
     * 
     */
    public List<OrderStatusType> getOrderStatus() {
        if (orderStatus == null) {
            orderStatus = new ArrayList<OrderStatusType>();
        }
        return this.orderStatus;
    }

    /**
     * Gets the value of the minimumExecuteVolume property.
     * 
     * @return
     *     possible object is
     *     {@link QuantityType }
     *     
     */
    public QuantityType getMinimumExecuteVolume() {
        return minimumExecuteVolume;
    }

    /**
     * Sets the value of the minimumExecuteVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityType }
     *     
     */
    public void setMinimumExecuteVolume(QuantityType value) {
        this.minimumExecuteVolume = value;
    }

    /**
     * Gets the value of the triggerDetails property.
     * 
     * @return
     *     possible object is
     *     {@link TriggerDetailsType }
     *     
     */
    public TriggerDetailsType getTriggerDetails() {
        return triggerDetails;
    }

    /**
     * Sets the value of the triggerDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link TriggerDetailsType }
     *     
     */
    public void setTriggerDetails(TriggerDetailsType value) {
        this.triggerDetails = value;
    }

    /**
     * Gets the value of the undisclosedVolume property.
     * 
     * @return
     *     possible object is
     *     {@link QuantityType }
     *     
     */
    public QuantityType getUndisclosedVolume() {
        return undisclosedVolume;
    }

    /**
     * Sets the value of the undisclosedVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityType }
     *     
     */
    public void setUndisclosedVolume(QuantityType value) {
        this.undisclosedVolume = value;
    }

    /**
     * Gets the value of the orderDuration property.
     * 
     * @return
     *     possible object is
     *     {@link OrderDurationType }
     *     
     */
    public OrderDurationType getOrderDuration() {
        return orderDuration;
    }

    /**
     * Sets the value of the orderDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderDurationType }
     *     
     */
    public void setOrderDuration(OrderDurationType value) {
        this.orderDuration = value;
    }

}
